#if !defined(__TOOL_H)
#define __TOOL_H

#include <nori/object.h>

NORI_NAMESPACE_BEGIN

/**
 * \brief Superclass of all phase functions
 */
class Tool : public NoriObject {
public:



	virtual void runTool() = 0;


	void activate(){

		cout << endl;
		cout << "Configuration: " << qPrintable(toString()) << endl;
		cout << endl;

		runTool();

	};

/**
	 * \brief Return the type of object (i.e. Mesh/PHASE/etc.) 
	 * provided by this instance
	 * */
	EClassType getClassType() const { return ETool; }
};

NORI_NAMESPACE_END

#endif /* __TOOL_H */