#if !defined(__CORE_H)
#define __CORE_H

#include <nori/object.h>

NORI_NAMESPACE_BEGIN

class Core{

public:

	static void init();

};

NORI_NAMESPACE_END

#endif
