#if !defined(__VOLUME_H)
#define __VOLUME_H

#include <nori/object.h>

NORI_NAMESPACE_BEGIN

//template <class T>
class VolumeData: public NoriObject{

public:

	virtual ~VolumeData(){};


	virtual float lookupData(const Point3f &p)const = 0;


	EClassType getClassType() const { return EVolumeData; }


protected:
//	VolumeData();

};


NORI_NAMESPACE_END

#endif /* __MEDIUM_H */
