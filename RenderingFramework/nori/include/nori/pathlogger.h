#if !defined(__PATHLOGGER_H)

#define __PATHLOGGER_H

#include <vector>
#include <string>
#include <iostream>

#include <nori/ray.h>

#include <nori/luminaire.h>
#include <nori/bsdf.h>
#include <nori/mesh.h>


class PathSegment{

public:
	enum SEG_TYPE{
		PRIMARY,
		DIFFUSE,
		SHADOW,
		INVALID

	};

	PathSegment(){
	};

	PathSegment(nori::Ray3f ray, float d,int t){
		pos[0] = ray.o[0];	
		pos[1] = ray.o[1];
		pos[2] = ray.o[2];

		direct[0] = ray.d[0];	
		direct[1] = ray.d[1];
		direct[2] = ray.d[2];
		dist = d;
		type = t;

	};

	float pos[3];
	float direct[3];
	float dist;
	int type;
};

//typedef std::vector<PathSegment> Path;
template <class T>
class Path{
public:
	~Path(){
		std::cout <<"~Path"<<std::endl;
		pseg.clear();
	}
public:

	std::vector<T> pseg;
	int length;

};


class PathLogger{



public:

	enum PL_MODE{
		PL_WRITE,
		PL_READ
	};

	virtual void init(int mode, std::string filename,int res[]) = 0;

	virtual void finit() = 0;
	
	virtual void nextPixel(int pxIdx)=0;

	virtual void nextPath(int pathIdx)=0;

	//virtual void addPathSegment(nori::Ray3f ray, float d,int t)=0;
	virtual void addPathSegment(nori::BSDFQueryRecord &bRec, nori::LuminaireQueryRecord &lRec, nori::Intersection &its)=0;
	//virtual std::vector<Path<>*> getPathsPerPixel(int pxIdx) = 0;

};


class EmptyPathLogger: public PathLogger{

public:
	/*
	EmptyPathLogger(){
		std::cout <<"EmptyPathLogger"<<std::endl;
	}
	*/
	void init(int mode, std::string filename,int res[]){};

	void finit(){};
	
	void nextPixel(int pxIdx){};

	void nextPath(int pathIdx){};

	void addPathSegment(nori::BSDFQueryRecord &bRec, nori::LuminaireQueryRecord &lRec, nori::Intersection &its){};

	std::vector<Path<PathSegment>*> getPathsPerPixel(int pxIdx){std::vector<Path<PathSegment>*> p;
													return p;};
};

#endif /* __PATHLOGGER_H */
