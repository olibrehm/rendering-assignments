/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#if !defined(__PARSER_H)
#define __PARSER_H

#include <nori/object.h>

#include <Eigen/Geometry>
#include <QtGui>
#include <QtXml>
#include <QtXmlPatterns>
#include <stack>

NORI_NAMESPACE_BEGIN

/**
 * \brief Load a scene from the specified filename and
 * return its root object
 */
extern NoriObject *loadScene(const QString &filename);


class NoriParser : public QXmlDefaultHandler {
public:
	/// Set of supported XML tags
	enum ETag {
		/* Object classes */
		EScene                = NoriObject::EScene,
		ESubScene             = NoriObject::ESubScene,
		EMesh                 = NoriObject::EMesh,
		EBSDF                 = NoriObject::EBSDF,
		ELuminaire            = NoriObject::ELuminaire,
		ECamera               = NoriObject::ECamera,
		EMedium               = NoriObject::EMedium,
		EPhaseFunction        = NoriObject::EPhaseFunction,
		EIntegrator           = NoriObject::EIntegrator,
		ESampler              = NoriObject::ESampler,
		ETest                 = NoriObject::ETest, 
		EReconstructionFilter = NoriObject::EReconstructionFilter,
		EVolumeData			  = NoriObject::EVolumeData,
		ERenderManager		  = NoriObject::ERenderManager,
		EAccelerator		  = NoriObject::EAccelerator,
		ETool				  = NoriObject::ETool,
		ETexture			  = NoriObject::ETexture,

		/* Properties */
		EBoolean = NoriObject::EClassTypeCount,
		EInteger,
		EFloat,
		EString,
		EPoint,
		EVector,
		EColor,
		ETransform,
		ETranslate,
		ERotate,
		EScale,
		ELookAt,
		EMatrix
	};

	NoriParser();

	struct ParserContext {
		QXmlAttributes attr;
		PropertyList propList;
		std::vector<NoriObject *> children;

		inline ParserContext(const QXmlAttributes &attr) : attr(attr) { }
	};


	float parseFloat(const QString &str) const;
	Vector3f parseVector(const QString &str) const;
	Eigen::Matrix4f parseMatrix(const QString &str) const;
	bool startElement(const QString & /* unused */, const QString &name,
		const QString& /* unused */, const QXmlAttributes &attr);

bool endElement(const QString & /* unused */, const QString &name,
			const QString& /* unused */);


	inline NoriObject *getRoot() const {
		return m_root;
	};

	private:
	std::map<QString, ETag> m_tags;
	std::vector<ParserContext> m_context;
	Eigen::Affine3f m_transform;
	NoriObject *m_root;
};


class NoriMessageHandler : public QAbstractMessageHandler {
public:
	void handleMessage(QtMsgType type, const QString &descr, 
			const QUrl &, const QSourceLocation &loc);

};

NORI_NAMESPACE_END

#endif /* __PARSER_H */
