#if !defined(__RENDERMANAGER_H)
#define __RENDERMANAGER_H

#include <QMutex>
#include <QThread>
#include <QWidget>

#include <QCloseEvent>
#include <QApplication>

#include <nori/common.h>
#include <nori/object.h>
//#include <nori/scene.h>
#include <nori/imgblock.h>

#include <QElapsedTimer>

//class Scene;

NORI_NAMESPACE_BEGIN

class RenderManager: public NoriObject, public QThread{

public:
	
	//RenderManager(Scene *scene, Sampler *sampler, ImageBlock *result, int nCores );
	virtual void setup(Scene *scene, Sampler *sampler, ImageBlock *result, int nCores )=0;

	void setWindow(QWidget *window, bool closeWhenDone){
		m_window = window;
		m_cwd = closeWhenDone;
	};

	virtual void manage()=0;

	virtual void run(){
		m_stop = false;

		m_timer.start();

		manage();


		cout << "Rendering finished (took " << m_timer.elapsed() << " ms)" << endl;

		if (m_cwd){
			if(m_window!=NULL){
				//m_window->close();
				QEvent* cevent = new QCloseEvent();
				QApplication::postEvent (m_window, cevent);
			}
		}
	};


	virtual void stop(){m_stop=true;}


	EClassType getClassType() const { return ERenderManager; }

protected:
	Scene *m_scene;
	Sampler *m_sampler;
	ImageBlock *m_result;

	int m_nCores;

	QWidget *m_window;
	bool m_cwd;

	QElapsedTimer m_timer;

	bool m_stop;
};

class ProgressiveRenderManager: public RenderManager{
public:	
	ProgressiveRenderManager(const PropertyList &propList);
	
	void setup(Scene *scene, Sampler *sampler, ImageBlock *result, int nCores );

	virtual void manage();

	QString toString() const;

private:
	QString m_filename;
	int m_store;
	int m_nextStore;
};


class IterativeRenderManager: public RenderManager{
public:	
	IterativeRenderManager(const PropertyList &propList) {};

	void setup(Scene *scene, Sampler *sampler, ImageBlock *result, int nCores );
	
	virtual void manage(); 

	QString toString() const;

private:

	QString generateOutputName(QString sceneName ,int iteration);

};

NORI_NAMESPACE_END

#endif /*__RENDERMANAGER_H*/
