/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#if !defined(__BSDF_H)
#define __BSDF_H

#include <nori/object.h>

#include <nori/mesh.h>

#include <nori/sampler.h>

NORI_NAMESPACE_BEGIN


/**
 * \brief Convenience data structure used to pass multiple
 * parameters to the evaluation and sampling routines in \ref BSDF
 */
struct BSDFQueryRecord {
	/// Incident direction (in the local frame)
	Vector3f wi;

	/// Outgoing direction (in the local frame)
	Vector3f wo;

	/// Measure associated with the sample
	EMeasure measure;

	float pdf;

	Color3f bsdfWeight;

	int bsdfType;

	int bsdfEvent;
	//float eta;

	Intersection its;

	//Sampler* sampler;

	inline BSDFQueryRecord(EMeasure measure):measure(measure),pdf(1.0f){}

	inline BSDFQueryRecord():pdf(1.0f){}

	/// Create a new record for sampling the BSDF
	inline BSDFQueryRecord(const Vector3f &wi)
		: wi(wi), measure(EUnknownMeasure),pdf(1.0f){ }

	/// Create a new record for querying the BSDF
	inline BSDFQueryRecord(const Vector3f &wi,
			const Vector3f &wo, EMeasure measure)
		: wi(wi), wo(wo), measure(measure) ,/*eta(1.0f),*/ pdf(1.0f){ }
};

/**
 * \brief Superclass of all bidirectional scattering distribution functions
 */
class BSDF : public NoriObject {

public:
	
	enum EBSDFType
	{
		ENull                 = 0x00001, // no deflection of photons at the surface
		EDiffuseReflection    = 0x00002, // ideal diffuse reflection
		EDiffuseTransmission  = 0x00004, // ideal diffuse transmission
		EDeltaReflection      = 0x00020, // perfect reflection
		EDeltaTransmission    = 0x00040  // perfect transmission
	};
	
	enum EBSDFTypeCombinations
	{
		EReflection   = EDiffuseReflection | EDeltaReflection,         // any reflection
		ETransmission = EDiffuseTransmission | EDeltaTransmission,     // any reflection
		EDelta        = ENull | EDeltaReflection | EDeltaTransmission, // all perfect interactions
		EDiffuse      = EDiffuseReflection,                            // all ideal diffuse interactions
		ESmooth       = EDiffuseReflection,                            // all non-perfect/smooth interactions
		EAll          = ESmooth | EDelta                               // all possible interactions
	};


public:


	/**
	 * \brief Evaluate the BSDF for a pair of directions and measure
	 * specified in \code bRec
	 * 
	 * \param bRec
	 *     A record with detailed information on the BSDF query
	 * \return
	 *     The BSDF value, evaluated for each color channel
	 */
	virtual Color3f eval(const BSDFQueryRecord &bRec) const{
		std::cout << "!! BSDF::eval(): Not Implemented !!" << std::endl;
	}



	/**
	 * \brief Sample the BSDF and return the importance weight (i.e. the
	 * value of the BSDF * cos(theta_o) divided by the probability density
	 * of the sample with respect to solid angles).
	 *
	 * \param bRec    A BSDF query record
	 * \param sampler  A pointer to a sampler which can generate uniform samples in 1D or 2D \f$[0,1]\f$
	 *
	 * \return The BSDF value divided by the probability density of the sample
	 *         sample. The returned value also includes the cosine
	 *         foreshortening factor associated with the outgoing direction,
	 *         when this is appropriate. A zero value means that sampling
	 *         failed.
	 */
	virtual Color3f sample(BSDFQueryRecord &bRec, Sampler *sampler) const{
		std::cout << "!! BSDF::sample(): Not Implemented !!" << std::endl;
	}


	/**
	 * \brief Compute the probability of sampling \c bRec.wo
	 * (conditioned on \c bRec.wi).
	 *
	 * This method provides access to the probability density that
	 * is realized by the \ref sample() method.
	 *
	 * \param bRec
	 *     A record with detailed information on the BSDF query
	 *
	 * \return
	 *     A probability/density value expressed with respect
	 *     to the specified measure
	 */

	virtual float pdf(const BSDFQueryRecord &bRec) const{
		std::cout << "!! BSDF::pdf(): Not Implemented !!" << std::endl;
	}


	
	/**
	 * \brief Return the type of object (i.e. Mesh/BSDF/etc.) 
	 * provided by this instance
	 * */
	EClassType getClassType() const { return EBSDF; }
};

NORI_NAMESPACE_END

#endif /* __BSDF_H */
