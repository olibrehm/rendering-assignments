/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#if !defined(__INTEGRATOR_H)
#define __INTEGRATOR_H

#include <nori/object.h>
#include <nori/dpdf.h>
#include <nori/frame.h>

#include <nori/pathlogger.h>
//#include <RtVTK/RL/RayLogger.h>

NORI_NAMESPACE_BEGIN

/**
 * \brief Abstract integrator (i.e. a rendering technique)
 *
 * In Nori, the different rendering techniques are collectively referred to as 
 * integrators, since they perform integration over a high-dimensional
 * space. Each integrator represents a specific approach for solving
 * the light transport equation---usually favored in certain scenarios, but
 * at the same time affected by its own set of intrinsic limitations.
 */
 
//template <class T>
class Integrator : public NoriObject {
public:

	Integrator(const PropertyList &propList):m_pathLogger(0){
//		m_progressive = propList.getBoolean("progressive", false);
		m_pathLogger = new EmptyPathLogger();
	}

	/// Release all memory
	virtual ~Integrator() { }

	/**
	 * \brief Sample the incident radiance along a ray
	 *
	 * \param scene
	 *    A pointer to the underlying scene
	 * \param sampler
	 *    A pointer to a sample generator
	 * \param ray
	 *    The ray in question
	 * \return
	 *    A (usually) unbiased estimate of the radiance in this direction
	 */
	virtual Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const = 0;

	virtual void prePass(const Scene *scene, Sampler *sampler) const {}


	//virtual IntegratorSample Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const{

	//	IntegratorSample is;
	//	is.Li = Li(scene, sampler, ray);
	//	return is;
	//};

/*
	bool isProgressive()const{
		return m_progressive;
	}
*/

	void setLogger(PathLogger* logger){m_pathLogger = logger;}

	/**
	 * \brief Return the type of object (i.e. Mesh/BSDF/etc.) 
	 * provided by this instance
	 * */
	EClassType getClassType() const { return EIntegrator; }

private:
	
//	bool m_progressive;


protected:
	PathLogger* m_pathLogger;
};

NORI_NAMESPACE_END

#endif /* __INTEGRATOR_H */
