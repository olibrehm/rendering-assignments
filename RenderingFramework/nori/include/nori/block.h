/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/* =======================================================================
     This file contains classes for parallel rendering of "image blocks".
 * ======================================================================= */

#if !defined(__PARALLEL_H)
#define __PARALLEL_H

//#include <nori/color.h>
#include <nori/vector.h>
#include <nori/imgblock.h>
#include <nori/rendermanager.h>


#include <QMutex>
#include <QThread>
#include <QElapsedTimer>




#define NORI_BLOCK_SIZE 32 /* Block size used for parallelization */

NORI_NAMESPACE_BEGIN



/**
 * \brief Spiraling block generator
 *
 * This class can be used to chop up an image into many small
 * rectangular blocks suitable for parallel rendering. The blocks
 * are ordered in spiraling pattern so that the center is
 * rendered first.
 */
class BlockGenerator {
public:
	/**
	 * \brief Create a block generator with
	 * \param size
	 *      Size of the image that should be split into blocks
	 * \param blockSize
	 *      Maximum size of the individual blocks
	 */
	BlockGenerator(const Vector2i &size, int blockSize, int iterations = 1);
	
	/**
	 * \brief Return the next block to be rendered
	 *
	 * This function is thread-safe
	 *
	 * \return \c false if there were no more blocks
	 */
	bool next(ImageBlock &block);
protected:
	enum EDirection { ERight = 0, EDown, ELeft, EUp };

	Point2i m_block;
	Vector2i m_numBlocks;
	Vector2i m_size;
	int m_blockSize;
	int m_numSteps;
	int m_blocksLeft;
	int m_stepsLeft;
	int m_direction;
	int m_blocksPerIteration;
	int m_numIterations;
	int m_iteration;
	QMutex m_mutex;
	//QElapsedTimer m_timer;
};



class RenderThread : public QThread {
public:
	/**
	 * \brief Create a new rendering thread that fetches blocks from
	 * the specified block generator and writes output to a big
	 * \ref ImageBlock instance that represents the entire image
	 */
	//RenderThread(const Scene *scene, Sampler *sampler,
	//	BlockGenerator *blockGenerator, ImageBlock *output);

	/// Release all memory
	//virtual ~RenderThread();

	/// Main rendering thread loop
	void run() = 0;
/*
protected:
	const Scene *m_scene;
	BlockGenerator *m_blockGenerator;
	ImageBlock *m_output;
	Sampler *m_sampler;
	*/
};

/**
 * \brief Render thread
 *
 * This class implements the main rendering logic, which consists of
 * fetching work from a scheduler (in the form of rectangular image
 * blocks to be rendered), processing it, and writing the output
 * to a target buffer.
 */
class BlockRenderThread : public RenderThread {
public:
	/**
	 * \brief Create a new rendering thread that fetches blocks from
	 * the specified block generator and writes output to a big
	 * \ref ImageBlock instance that represents the entire image
	 */
	BlockRenderThread(const Scene *scene, Sampler *sampler,
		BlockGenerator *blockGenerator, ImageBlock *output, int numSamples);

	/// Release all memory
	virtual ~BlockRenderThread();

	/// Main rendering thread loop
	void run();

private:
	const Scene *m_scene;
	BlockGenerator *m_blockGenerator;
	ImageBlock *m_output;
	Sampler *m_sampler;
	int m_numSamples;
};


class BlockRenderManager: public RenderManager{

public:

	BlockRenderManager(const PropertyList &propList) {};

	void setup(Scene *scene, Sampler *sampler, ImageBlock *result, int nCores );


	virtual void manage();

	QString toString() const;
};


NORI_NAMESPACE_END

#endif /* __PARALLEL_H */
