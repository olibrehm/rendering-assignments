#if !defined(__TEXTURE_H_)
#define __TEXTURE_H_
#include <nori/object.h>
#include <nori/color.h>
#include <nori/vector.h>

#include <nori/mesh.h>

NORI_NAMESPACE_BEGIN

class Texture2D: public NoriObject{

public:

	Texture2D(const PropertyList &propList){
		m_uvoffset = Point2f(propList.getFloat("uoffset",0.0f),propList.getFloat("voffset",0.0f));

		float uvscale = propList.getFloat("uvscale",1.0f);
		m_uvscale = Vector2f(propList.getFloat("uscale",uvscale), propList.getFloat("vscale",uvscale));

	}

	virtual Color3f eval(const Intersection &its ,bool filter = true)const =0;

	//virtual Vector3i getResolution()const;

	EClassType getClassType() const { return ETexture; }

protected:
	inline Point2f getFinalUV(const Point2f &uv)const{
		return uv.cwiseProduct(m_uvscale)+m_uvoffset;
	}

	//offsets in the uv space
	Point2f m_uvoffset;
	

	//scale in the uvspace
	Vector2f m_uvscale;


};

/*
class Texture2D: public Texture{

}
*/

NORI_NAMESPACE_END

#endif /* __TEXTURE_H_ */