#if !defined(__IMGBLOCK_H)
#define __IMGBLOCK_H


#include <nori/color.h>
#include <nori/vector.h>
#include <QMutex>

NORI_NAMESPACE_BEGIN

/**
 * \brief Weighted pixel storage for a rectangular subregion of an image
 *
 * This class implements storage for a rectangular subregion of a
 * larger image that is being rendered. For each pixel, it records color
 * values along with a weight that specifies the accumulated influence of
 * nearby samples on the pixel (according to the used reconstruction filter).
 *
 * When rendering with filters, the samples in a rectangular
 * region will generally also contribute to pixels just outside of 
 * this region. For that reason, this class also stores information about
 * a small border region around the rectangle, whose size depends on the
 * properties of the reconstruction filter.
 */
class ImageBlock : public Eigen::Array<Color4f, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> {
public:
	/**
	 * Create a new image block of the specified maximum size
	 * \param size
	 *     Desired maximum size of the block
	 * \param filter
	 *     Samples will be convolved with the image reconstruction
	 *     filter provided here.
	 */
	ImageBlock(const Vector2i &size, const ReconstructionFilter *filter);
	
	/// Release all memory
	~ImageBlock();
	
	/// Configure the offset of the block within the main image
	void setOffset(const Point2i &offset) { m_offset = offset; }

	/// Return the offset of the block within the main image
	inline const Point2i &getOffset() const { return m_offset; }
	
	/// Configure the size of the block within the main image
	void setSize(const Point2i &size) { m_size = size; }

	/// Return the size of the block within the main image
	inline const Vector2i &getSize() const { return m_size; }

	/// Return the border size in pixels
	inline int getBorderSize() const { return m_borderSize; }

	/**
	 * \brief Turn the block into a proper bitmap
	 * 
	 * This entails normalizing all pixels and discarding
	 * the border region.
	 */
	Bitmap *toBitmap() const;

	/// Clear all contents
	void clear() { setConstant(Color4f()); }

	/// Record a sample with the given position and radiance value
	void put(const Point2f &pos, const Color3f &value);


	/// Record a sample with the given position and radiance value. No image Reconstruction algorithm is used.
	void putNoFilter(const Point2f &pos, const Color3f &value);
	/**
	 * \brief Merge another image block into this one
	 *
	 * During the merge operation, this function locks 
	 * the destination block using a mutex.
	 */
	void put(ImageBlock &b);

	/// Lock the image block (using an internal mutex)
	inline void lock() const { m_mutex.lock(); }
	
	/// Unlock the image block
	inline void unlock() const { m_mutex.unlock(); }

	/// Return a human-readable string summary
	QString toString() const;
protected:
	Point2i  m_offset;
	Vector2i m_size;
	int m_borderSize;
	float *m_filter, m_filterRadius;
	float *m_weightsX, *m_weightsY;
	float m_lookupFactor;
	mutable QMutex m_mutex;
};


NORI_NAMESPACE_END

#endif /* __IMGBLOCK_H */
