
#ifndef __LUMINAIRE_H
#define __LUMINAIRE_H

//#include <nori/mesh.h>
#include <nori/object.h>

NORI_NAMESPACE_BEGIN

//class Luminaire;

struct LuminaireQueryRecord {

	//the sample direction in which the luminaire emitts light (local emitter surface space)
	Vector3f wi;

	//the sample direction in which the luminaire emitts light (world space)
	Vector3f ws_wi;

	//position in ws of the sample on the luminaire
	//Point3f p;

	//position in ws of the sample
	Point3f sp;

	//position in ws of the sample on the luminaire
	Point3f lp;

	//the pdf for sampling theis position on the luminaire
	float pdf;

	//the distance from the sample point to the light source
	float distance;
	
	// Measure associated with the sample
	EMeasure measure;

	//radiance emitted by the light source
	Color3f radiance;

	//index of the luminaire in the luminaire list of the scene
	int lidx;

	inline LuminaireQueryRecord():pdf(1.0),lidx(-1){
		
	};

	/// Create a new record for sampling the Luminaire
	inline LuminaireQueryRecord(const Vector3f &wi)
		: wi(wi), measure(EUnknownMeasure),pdf(1.0) { }

};


class Luminaire: public NoriObject{

public:

	virtual ~Luminaire() { }

	
	virtual Color3f eval(LuminaireQueryRecord &lRec) const = 0;
	
	/*
	virtual Color3f sample(LuminaireQueryRecord &lRec, Point2f sample) const = 0;

	virtual float pdf(LuminaireQueryRecord &lRec) const = 0;
	*/

	/** \brief Sample the luminaire direct.
	*
	*/
	virtual Color3f sampleDirect(LuminaireQueryRecord &lRec, Point2f sample) const = 0;

	/** \brief Compute the probability of sampling the luminaire direct.
	*
	*/
	virtual float pdfDirect(LuminaireQueryRecord &lRec) const = 0;
	/**/

	virtual Color3f sampleLightRay(LuminaireQueryRecord &lRec, Sampler *sampler) const = 0;


	//virtual void samplePosition(LuminaireQueryRecord &lRec, Point2f sample) const =0;

	//virtual float pdfPosition(LuminaireQueryRecord &lRec) const = 0;


	/**
	 * \brief Return the type of object (i.e. Mesh/BSDF/etc.) 
	 * provided by this instance
	 * */
	EClassType getClassType() const { return ELuminaire; }

	//virtual void setParent(Mesh* parent) = 0;

	//virtual float surfaceArea()const = 0;

	virtual Color3f power()const =0;

public:
	int idx;


private:

	//Mesh *m_parent;

};

NORI_NAMESPACE_END

#endif
