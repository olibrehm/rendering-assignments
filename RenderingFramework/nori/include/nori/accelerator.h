#if !defined(_ACCELERATOR_H)

#include <nori/object.h>
#include <nori/bbox.h>

#include <nori/ray.h>
#include <nori/mesh.h>
#define _ACCELERATOR_H

NORI_NAMESPACE_BEGIN

class Accelerator: public NoriObject{

public:
	/**
	 * \brief Register a triangle mesh for inclusion in the kd-tree.
	 *
	 * This function can only be used before \ref build() is called
	 */
	virtual void addMesh(Mesh *mesh)=0;

	/// Build the kd-tree
	virtual void build()=0;

	virtual bool rayIntersect(const Ray3f &ray, Intersection &its, 
		bool shadowRay = false) const=0;

	//// Return an axis-aligned bounding box containing the entire tree
	virtual const BoundingBox3f &getBoundingBox() const = 0;

	/// Return a human-readable summary of this instance
	//QString toString() const;

	/**
	 * \brief Return the type of object (i.e. Mesh/BSDF/etc.) 
	 * provided by this instance
	 * */

	EClassType getClassType() const { return EAccelerator; }

protected:
	//using Parent::m_bbox;
};



NORI_NAMESPACE_END

#endif