#include <nori/luminaire.h>
#include <nori/bitmap.h>

#include <nori/frame.h>

#include <nori/mesh.h>

#include <nori/sampler.h>

NORI_NAMESPACE_BEGIN

class PointLight: public Luminaire{
public:
	PointLight(const PropertyList &propList){
		m_intensity = propList.getColor("intensity");
		m_toWorld = propList.getTransform("toWorld");
	}
	~PointLight(){
	}

	Color3f sampleDirect(LuminaireQueryRecord &lRec, Point2f sample) const{
		
		lRec.lp = m_toWorld*Point3f(0.0f);
		lRec.pdf = 1.0f;
		lRec.measure = EDiscrete;
		Vector3f ld = lRec.sp - lRec.lp;
		lRec.distance = sqrt(ld.dot(ld));
		float invDist = 1.0f/ lRec.distance;
		ld.normalize();
		lRec.ws_wi = ld;
		lRec.wi = ld;

		return m_intensity*(invDist*invDist);

		
	}

	Color3f sampleLightRay(LuminaireQueryRecord &lRec, Sampler *sampler) const
	{
		lRec.wi = squareToUniformSphere(sampler->next2D());
		lRec.ws_wi = lRec.wi;
		lRec.measure = EDiscrete;

		lRec.pdf = this->pdfDirect(lRec);

		lRec.ws_wi = lRec.wi;

		return this->power();
	}

	float pdfDirect(LuminaireQueryRecord &lRec)const{
		if(lRec.measure == EDiscrete){
			return 1.0f;
		}else{
			return 0.0f;
		}
	}


	Color3f eval(LuminaireQueryRecord &lRec)const{

		return Color3f(0.0);
		//return m_intensity/(4.0*M_PI);
	}

	float surfaceArea()const{
		return 1.0f;
	}

	Color3f power()const{
		return m_intensity*4.0*M_PI;
	}

	QString toString() const {
		return QString(
			"PointLight[\n"
			"  intensity = %1,\n"
			"]")
			.arg(m_intensity.toString());
	}

private:
	Color3f m_intensity;
	Transform m_toWorld;
};

NORI_REGISTER_CLASS(PointLight, "point");
NORI_NAMESPACE_END
