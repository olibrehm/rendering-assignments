/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/bsdf.h>
#include <nori/frame.h>
#include <nori/texture.h>

NORI_NAMESPACE_BEGIN

/**
 * \brief Diffuse / Lambertian BRDF model
 */
class Diffuse : public BSDF {
public:
	Diffuse(const PropertyList &propList) {
		m_albedo = propList.getColor("albedo", Color3f(0.5f));
		m_albedoTexture = NULL;
	}

	/// Evaluate the BRDF model
	Color3f eval(const BSDFQueryRecord &bRec) const {
		/* This is a smooth BRDF -- return zero if the measure
		   is wrong, or when queried for illumination on the backside */
		if (bRec.measure != ESolidAngle
			|| Frame::cosTheta(bRec.wi) <= 0
			|| Frame::cosTheta(bRec.wo) <= 0
			) {
			return Color3f(0.0f);
		}

		Color3f c;
		if(m_albedoTexture != NULL) {
			c = m_albedoTexture->eval(bRec.its, false);
		} else {
			c = m_albedo;
		}

		/* The BRDF is simply the albedo / pi */
		return c * INV_PI;
	}

	Color3f sample(BSDFQueryRecord &bRec, Sampler *sampler) const{
		// sample cosine hemisphere
		bRec.wo = squareToCosineHemisphere(sampler->next2D());
		bRec.pdf = this->pdf(bRec);
		bRec.measure = ESolidAngle;

		return (this->eval(bRec) * Frame::cosTheta(bRec.wo)) / this->pdf(bRec);
	}

	float pdf(const BSDFQueryRecord &bRec) const{
		// pdf of cosine hemisphere is cos(theta) / pi. See http://www.rorydriscoll.com/2009/01/07/better-sampling/
		return Frame::cosTheta(bRec.wo) * INV_PI;
	}

	void addChild(NoriObject *child) {
		if(child->getClassType() == ETexture) {
			m_albedoTexture = static_cast<Texture2D*>(child);
		}
	}

	/// Return a human-readable summary
	QString toString() const {
		return QString(
			"Diffuse[\n"
			"  albedo = %1\n"
			"]").arg(m_albedo.toString());
	}

	EClassType getClassType() const { return EBSDF; }
private:
	Color3f m_albedo;
	Texture2D* m_albedoTexture;
};

NORI_REGISTER_CLASS(Diffuse, "diffuse");
NORI_NAMESPACE_END
