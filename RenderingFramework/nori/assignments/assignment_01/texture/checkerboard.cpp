#include <nori/texture.h>

NORI_NAMESPACE_BEGIN

class CheckerBoardTexture2D: public Texture2D{

public:
	CheckerBoardTexture2D(const PropertyList &propList):Texture2D(propList){
		m_color0 = propList.getColor("color0",Color3f(0.0f));
		m_color1 = propList.getColor("color1",Color3f(1.0f));
		m_tiles = propList.getInteger("tiles", 10);
	}

	inline float modulo(float a, float b)const {
		float r = std::fmod(a, b);
		return (r < 0) ? r+b : r;
	}

	Color3f eval(const Intersection &its ,bool filter = true)const{
		Point2f uv = getFinalUV(its.uv);

		int x = (int) (m_tiles * uv[0]);
		int y = (int) (m_tiles * uv[1]);

		bool c0;

		if(x % 2 != 0) {
			if(y % 2 != 0) {
				c0 = true;
			} else {
				c0 = false;
			}
		} else {
			if(y % 2 != 0) {
				c0 = false;
			}
			else {
				c0 = true;
			}
		}
		
		return c0 ? m_color0 : m_color1;
	}


	QString toString()const {
		return QString(
				"Checkerboard[\n"
				"  color0 = %1,\n"
				"  color1 = %2\n"
				"]")
				.arg(m_color0.toString())
				.arg(m_color1.toString());
	}
private:
	Color3f m_color0;
	Color3f m_color1;
	int m_tiles;

};
NORI_REGISTER_CLASS(CheckerBoardTexture2D, "checkerboard");

NORI_NAMESPACE_END
