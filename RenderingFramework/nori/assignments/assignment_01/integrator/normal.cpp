/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/scene.h>

NORI_NAMESPACE_BEGIN


class NormalIntegrator : public Integrator {
public:
	NormalIntegrator(const PropertyList &propList):Integrator(propList) {

	}

	Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
		/* Find the surface that is visible in the requested direction */
		Intersection its;
		if (!scene->rayIntersect(ray, its)){

			return Color3f(0.0f);
		}

		/* the normal direction in the local coordinate frame */
		Vector3f n = Vector3f(0.0,0.0,1.0);

		/* Use the shading frame at "its" to convert it to world coordinates */
		n = its.toWorld(n);


		/* color code the normal values*/
		Color3f cn = Color3f(n[0],n[1],n[2]);
		cn+=1.0;
		cn/=2.0;

		return cn;
	
	}

	QString toString() const {
		return QString("NormalIntegrator");
	}
private:
	//float m_length;
};

NORI_REGISTER_CLASS(NormalIntegrator, "normal");
NORI_NAMESPACE_END
