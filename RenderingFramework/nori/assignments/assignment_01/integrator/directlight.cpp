/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/scene.h>

NORI_NAMESPACE_BEGIN


class DirectLightIntegrator : public Integrator {
public:
	DirectLightIntegrator(const PropertyList &propList):Integrator(propList) {
		srand (static_cast <unsigned> (time(0)));
	}

	Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
		/* Find the surface that is visible in the requested direction */
		Intersection its;
		if (!scene->rayIntersect(ray, its)){

			return Color3f(0.0f);
		}

		const Luminaire *luminaire = scene->getLuminaires()[0];

		LuminaireQueryRecord lQuery;
		lQuery.sp = its.p;

		Color3f cLight = luminaire->sampleDirect(lQuery, Point2f(0, 0));

		// shoot a ray to determine shadow of direct lighting
		Ray3f shadowRay(its.p, -lQuery.ws_wi);
		shadowRay.maxt = lQuery.distance; // distance intersection to light source

		if(scene->rayIntersect(shadowRay)) {
			// ray hit object on the way to light source
			return Color3f(0.0f);
		}

		const BSDF *bsdf = its.mesh->getBSDF();

		Vector3f wi = ray.d;
		Vector3f wo = its.toLocal(-lQuery.ws_wi);
		BSDFQueryRecord bsdfQuery(wi, wo , ESolidAngle);
		bsdfQuery.its = its;

		return cLight * bsdf->eval(bsdfQuery) * Frame::cosTheta(bsdfQuery.wo);
	}

	QString toString() const {
		return QString("DirectLightIntegrator");
	}
private:
	//float m_length;
};

NORI_REGISTER_CLASS(DirectLightIntegrator, "direct");
NORI_NAMESPACE_END
