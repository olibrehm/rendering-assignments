/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/scene.h>

NORI_NAMESPACE_BEGIN


class DepthIntegrator : public Integrator {
public:
	DepthIntegrator(const PropertyList &propList):Integrator(propList) {
		m_minDepth = propList.getFloat("minDepth", 0.0f);
		m_maxDepth = propList.getFloat("maxDepth", 1000.0f);
	}

	Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
		/* Find the surface that is visible in the requested direction */
		Intersection its;
		if (!scene->rayIntersect(ray, its)){

			return Color3f(0.0f);
		}

		float depth = its.t;

		if(depth > m_maxDepth )
		{
			depth = m_maxDepth;
		}

		if(depth < m_minDepth )
		{
			depth = m_minDepth;
		}

		depth = (depth - m_minDepth) / (m_maxDepth - m_minDepth);

		Color3f cd = Color3f(depth);

		return cd;
	
	}

	QString toString() const {
		return QString("DepthIntegrator");
	}
private:
	float m_minDepth;
	float m_maxDepth;
};

NORI_REGISTER_CLASS(DepthIntegrator, "depth");
NORI_NAMESPACE_END
