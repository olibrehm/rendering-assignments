/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/scene.h>

NORI_NAMESPACE_BEGIN


class AOIntegrator : public Integrator {
public:
	AOIntegrator(const PropertyList &propList):Integrator(propList) {
		m_dMax = propList.getFloat("dMax", 100.0f);
		m_nMax = propList.getInteger("nMax", 10);
		m_dExp = propList.getInteger("dExp", 5);

		srand (static_cast <unsigned> (time(0)));
	}

	Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
		/* Find the surface that is visible in the requested direction */
		Intersection its;
		if (!scene->rayIntersect(ray, its)){

			return Color3f(0.0f);
		}

		int n;
		float occluded;

		// generate square
		for(n = 0, occluded = 0; n < m_nMax; n++) {
			float x = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			float y = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			Vector3f outLocal = squareToUniformHemisphere(Point2f(x, y));
			Vector3f outWorld = its.toWorld(outLocal);

			Ray3f outRay(its.p, outWorld);
			outRay.maxt = m_dMax;

			Intersection secondIts;
			if(scene->rayIntersect(outRay, secondIts)) {
				float depth = secondIts.t / m_dMax;
				occluded += depth;
			}
		}

		float visibility = n - occluded;
		float ao = (float) visibility / n;

		//float depth = (its.t - m_minDepth) / (m_maxDepth - m_minDepth);
		//float smoothing = powf(depth, (float) m_dExp);

		Color3f cao = Color3f(/*smoothing* */ao);

		return cao;
	}

	QString toString() const {
		return QString("DepthIntegrator");
	}
private:
	float m_dMax;
	float m_nMax;
	float m_dExp;
};

NORI_REGISTER_CLASS(AOIntegrator, "ao");
NORI_NAMESPACE_END
