/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/scene.h>

NORI_NAMESPACE_BEGIN


class DirectLightPathIntegrator : public Integrator {
public:
	DirectLightPathIntegrator(const PropertyList &propList):Integrator(propList) {
		m_maxBounces = propList.getInteger("maxBounces", 50);
		m_rrMinBounces = propList.getInteger("rrMinBounces", 10);
	}

	Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
		Ray3f currentRay = ray;
		Color3f light(0.0f);
		Color3f throughput(1.0f);

		BSDFQueryRecord currendBSDFQuery;
		currendBSDFQuery.measure = EDiscrete;

		for(int i = 0; i< m_maxBounces; i++) {
			Intersection its;

			if (!scene->rayIntersect(currentRay, its)){
				break;
			}

			const BSDF *bsdf = its.mesh->getBSDF();

			if(its.mesh->getLuminaire() && currendBSDFQuery.measure == EDiscrete){
				Vector3f wi_its = its.toLocal(-currentRay.d);
				LuminaireQueryRecord lRec = LuminaireQueryRecord(wi_its);
				lRec.pdf = scene->pdfLuminaireDirect(lRec);
				lRec.distance = its.t;
				lRec.wi = wi_its;

				light += throughput *  its.mesh->getLuminaire()->eval(lRec);
			}

			// direct light contribution
			LuminaireQueryRecord lQuery;
			lQuery.sp = its.p;
			Color3f directLight = scene->sampleLuminairesDirect(its.p,
					lQuery, sampler);

			currendBSDFQuery.wi = its.toLocal(-currentRay.d);
			currendBSDFQuery.wo = its.toLocal(-lQuery.ws_wi);
			currendBSDFQuery.its = its;
			currendBSDFQuery.measure = ESolidAngle;
			Color3f directLightBSDF = bsdf->eval(currendBSDFQuery) *
					Frame::cosTheta(currendBSDFQuery.wo);

			//cout << "bsdf: " << directLightBSDF << ", light: " << directLight << endl;

			light += throughput * directLightBSDF * directLight;

			// shoot new ray
			currendBSDFQuery.wi = its.toLocal(-currentRay.d);
			currendBSDFQuery.its = its;
			currendBSDFQuery.measure = ESolidAngle;

			throughput *= bsdf->sample(currendBSDFQuery, sampler);

			currentRay = Ray3f(its.p, its.toWorld(currendBSDFQuery.wo));
		}

		// maximum of bounces reached
		return  light;
	}

	QString toString() const {
		return QString("DirectLightPathIntegrator");
	}
private:
	int m_maxBounces;
	int m_rrMinBounces;
};

NORI_REGISTER_CLASS(DirectLightPathIntegrator, "dlpath");
NORI_NAMESPACE_END

