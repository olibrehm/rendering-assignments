/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/scene.h>

NORI_NAMESPACE_BEGIN


class MultipleImportancePathIntegrator : public Integrator {
public:
	MultipleImportancePathIntegrator(const PropertyList &propList):Integrator(propList) {
		m_maxBounces = propList.getInteger("maxBounces", 50);
	}

	static float weightPower2(float primary, float secondary)
	{
		float p2 = primary * primary;
		float s2 = secondary * secondary;
		return p2 / (p2 + s2);
	}

	// returns the color by the direct lighting and the pdf of
	// the luminaire sampling and the pdf evaluation
	static Color3f directLightContrib(const Scene *scene,
			Sampler *sampler, Intersection &its, Vector3f wi,
			const BSDF *bsdf, float &pdfLight, float &pdfBsdf)
	{
		LuminaireQueryRecord lumQuery = LuminaireQueryRecord();
		lumQuery.sp = its.p;
		Color3f directLight = scene->sampleLuminairesDirect(its.p,
				lumQuery, sampler);
		pdfLight = lumQuery.pdf;

		BSDFQueryRecord bsdfQuery = BSDFQueryRecord();
		bsdfQuery.wi = wi;
		bsdfQuery.wo = its.toLocal(-lumQuery.ws_wi);
		bsdfQuery.its = its;
		bsdfQuery.measure = ESolidAngle;
		Color3f directLightBSDF = bsdf->eval(bsdfQuery) *
				Frame::cosTheta(bsdfQuery.wo);
		pdfBsdf = bsdf->pdf(bsdfQuery);

		//pdf *= bsdfQuery.pdf;

		return directLightBSDF * directLight;
	}

	Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
		Ray3f currentRay = ray;
		Color3f light(0.0f);
		Color3f throughput(1.0f);

		BSDFQueryRecord bsdfQuery;
		bsdfQuery.measure = EDiscrete;
		LuminaireQueryRecord lumQuery;

		for(int i = 0; i< m_maxBounces; i++) {
			Intersection its;

			const Luminaire *luminaire;
			if (!scene->rayIntersect(currentRay, its))
			{
				luminaire = scene->getEnvMap();
				if(luminaire != NULL)
				{
					// envmap contribution
					lumQuery = LuminaireQueryRecord(-currentRay.d);
					float wEnvmap = weightPower2(bsdfQuery.pdf, 0.0f);
					// direct light pdf is zero, only envmap here
					light += luminaire->eval(lumQuery) * throughput * wEnvmap;
				}
				break;
			}
			else
			{
				luminaire = its.mesh->getLuminaire();
			}

			const BSDF *bsdf = its.mesh->getBSDF();
			Vector3f wi_its = its.toLocal(-currentRay.d);

			// direct light contribution
			float directLightPdf;
			float directLightBsdfPdf;
			Color3f directLight = directLightContrib(scene, sampler, its,
					wi_its, bsdf, directLightPdf, directLightBsdfPdf);
			float wDirectLight = weightPower2(directLightPdf, directLightBsdfPdf);
			light += throughput * directLight * wDirectLight;

			// self emission contribution
			if( luminaire!=NULL )
			{
				LuminaireQueryRecord lRec = LuminaireQueryRecord(wi_its);
				lRec.lidx = luminaire->idx;
				lRec.pdf = scene->pdfLuminaireDirect(lRec);
				lRec.distance = its.t;
				lRec.wi = wi_its;
				if(bsdfQuery.measure == EDiscrete){
					lRec.pdf = 0.0;
				}
				float wEmissionLight = weightPower2(bsdfQuery.pdf, lRec.pdf);
				light += throughput * wEmissionLight * luminaire->eval(lRec);
			}

			// shoot new ray
			bsdfQuery.wi = wi_its;
			bsdfQuery.its = its;
			bsdfQuery.measure = ESolidAngle;

			throughput *= bsdf->sample(bsdfQuery, sampler);

			currentRay = Ray3f(its.p, its.toWorld(bsdfQuery.wo));
		}

		// maximum of bounces reached
		return light;
	}

	QString toString() const {
		return QString("MultipleImportancePathIntegrator");
	}
private:
	int m_maxBounces;
};

NORI_REGISTER_CLASS(MultipleImportancePathIntegrator, "mipath");
NORI_NAMESPACE_END

