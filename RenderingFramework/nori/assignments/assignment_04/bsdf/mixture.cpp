#include <nori/bsdf.h>
#include <nori/frame.h>
#include <nori/texture.h>

NORI_NAMESPACE_BEGIN

class Mixture : public BSDF {
public:
	Mixture(const PropertyList &propList) {
		m_weight = propList.getFloat("weight", 0.5f);
	}

	void addChild(NoriObject *child) {
		if(child->getClassType() == EBSDF) {
			if(m_bsdf1 == NULL) {
				m_bsdf1 = static_cast<BSDF*>(child);
			} else if(m_bsdf2 == NULL) {
				m_bsdf2 = static_cast<BSDF*>(child);
			} else {
				std::cerr << "Mixture BSDF cannot have more than two child BSDFs!"
						<< std::endl;
			}
		}
	}

	/// Evaluate the BRDF model
	Color3f eval(const BSDFQueryRecord &bRec) const {
		if(m_bsdf1 == NULL || m_bsdf2 == NULL) {
			return Color3f(0.0f);
		}

		return m_weight * m_bsdf1->eval(bRec) + (1.0f - m_weight) *
				m_bsdf2->eval(bRec);
	}

	Color3f sample(BSDFQueryRecord &bRec, Sampler *sampler) const {
		if(m_bsdf1 == NULL || m_bsdf2 == NULL) {
			return Color3f(0.0f);
		}

		if(sampler->next1D() < m_weight) {
			return m_bsdf1->sample(bRec, sampler);
		} else {
			return m_bsdf2->sample(bRec, sampler);
		}
	}

	float pdf(const BSDFQueryRecord &bRec) const {
		return 1.f;
	}

	/// Return a human-readable summary
	QString toString() const {
		return QString("Mixture[value = %1]").arg(m_weight);
	}


	EClassType getClassType() const { return EBSDF; }
private:
	float m_weight;

	BSDF *m_bsdf1 = NULL;
	BSDF *m_bsdf2 = NULL;
};

NORI_REGISTER_CLASS(Mixture, "mixture");
NORI_NAMESPACE_END

