/*
 * PhotonMap.cpp
 *
 *  Created on: Jan 27, 2016
 *      Author: parallels
 */

#include "PhotonMap.h"
#include <iostream>
#include <fstream>

NORI_NAMESPACE_BEGIN

PhotonMap::PhotonMap() : m_kdTreeBuilt(false) {
	// TODO Auto-generated constructor stub

}

PhotonMap::~PhotonMap() {
	this->clear();
}

void PhotonMap::addPhoton(Point3f position, Vector3f direction, Vector3f normal, Color3f intensity)
{
	m_photons.push_back(new Photon(position, direction, normal, intensity));
}

void PhotonMap::clear()
{
	for(int i = 0; i < m_photons.size(); i++) {
		delete m_photons[i];
	}
	m_photons.clear();
	m_kdTreeBuilt = false;
}

void PhotonMap::writePLY(std::string path)
{
	cout << "Writing photons to PLY point cloud..." << endl;

	std::ofstream plyFile;
	plyFile.open(path.c_str());
	plyFile << "ply\nformat ascii 1.0\n";

	plyFile << "element vertex " << m_photons.size() << "\n";

	plyFile << "property float x\n";
	plyFile << "property float y\n";
	plyFile << "property float z\n";

	plyFile << "property uchar diffuse_red\n";
	plyFile << "property uchar diffuse_green\n";
	plyFile << "property uchar diffuse_blue\n";

	plyFile << "end_header\n";

	for(int i = 0; i < m_photons.size(); i++) {
		Photon *p = m_photons[i];
		plyFile << p->position.x() << " ";
		plyFile << p->position.y() << " ";
		plyFile << p->position.z() << " ";

		plyFile << (int) fminf((float) p->intensity.x() * 256.0, 255.0) << " ";
		plyFile << (int) fminf((float) p->intensity.y() * 256.0, 255.0) << " ";
		plyFile << (int) fminf((float) p->intensity.z() * 256.0, 255.0) << "\n";
	}

	plyFile.close();

	cout << "Finished writing PLY." << endl;
}

void PhotonMap::buildKdTree()
{
	std::cout << "Building kd tree with " << m_photons.size() << " photons." << std::endl;

	m_kdProgress = 0;
	m_root= kdBalance(0, m_photons.size() - 1);

	cout << "Building Kd tree done" << endl;

	m_kdTreeBuilt = true;

	//writePLY("photons.ply");
	//printTree();
}

bool compareAxisX (Photon* i,Photon* j) { return (i->position.x()<j->position.x()); }
bool compareAxisY (Photon* i,Photon* j) { return (i->position.y()<j->position.y()); }
bool compareAxisZ (Photon* i,Photon* j) { return (i->position.z()<j->position.z()); }

Photon* PhotonMap::kdBalance(size_t start, size_t end)
{
	// find largest dimension
	int dim = kdFindLargestDimension(start, end); // x: 0, y: 1, y: 2

	// find median
    size_t n = (start + end) / 2;

    switch(dim) {
    case 0:
        std::nth_element(m_photons.begin() + start, m_photons.begin() + n, m_photons.begin() + end, compareAxisX);
        break;
    case 1:
        std::nth_element(m_photons.begin() + start, m_photons.begin() + n, m_photons.begin() + end, compareAxisY);
        break;
    case 2:
        std::nth_element(m_photons.begin() + start, m_photons.begin() + n, m_photons.begin() + end, compareAxisZ);
        break;
    default:
        break;
    }

    Photon *median = m_photons[n];
    median->axis = dim;

	m_kdProgress++;
	if(m_kdProgress % 10000 == 0) {
		cout << "KdTree Progress: " << (100 * m_kdProgress / m_photons.size()) << "%" << std::flush;
	}

	if(n > 0 && n > start + 1) {
		median->left = kdBalance(start, n - 1);
	}

	if(n < end - 1 && n < m_photons.size() - 1) {
		median->right = kdBalance(n + 1, end);
	}

	return median;
}

int PhotonMap::kdFindLargestDimension(size_t start, size_t end) // x: 0, y: 1, y: 2
{
	float minX = m_photons[start]->position.x(); float maxX = minX;
	float minY = m_photons[start]->position.y(); float maxY = minY;
	float minZ = m_photons[start]->position.z(); float maxZ = minZ;

	for(int i = start + 1; i <= end; i++) {
		Point3f p = m_photons[i]->position;

		if(p.x() < minX) minX = p.x();
		else if(p.x() > maxX) maxX = p.x();

		if(p.y() < minY) minY = p.y();
		else if(p.y() > maxY) maxY = p.y();

		if(p.z() < minZ) minZ = p.z();
		else if(p.z() > maxZ) maxZ = p.z();
	}

	float dimX = maxX - minX;
	float dimY = maxY - minY;
	float dimZ = maxZ - minZ;

	int dim = 0; // x: 0, y: 1, y: 2

	if(dimY > dimX) {
		dim = 1;
	}

	if(dimZ > dimX && dimZ > dimY) {
		dim = 2;
	}

	return dim;
}

Color3f PhotonMap::contributionAroundPoint(Point3f position, Vector3f normal, float maxDist)
{
	//return Color3f(fabsf(normal.x()), fabsf(normal.y()), fabsf(normal.z()));


	Color3f l = Color3f(0.0, 0.0, 0.0);

	kdSearchRange(maxDist, position, m_root, normal, l);

	float dA = (1.0 - 2.0 / (3.0 * CONE_K)) * M_PI * maxDist * maxDist;

	return l / dA;
}

void PhotonMap::kdSearchRange(float d, Point3f position, Photon *photon, Vector3f normal, Color3f &l)
{
	if(photon == NULL) return;

	if(photon->distanceTo(position) <= d) {
		Color3f light = photon->contributionTo(position, d);
		float normalContribution = fmaxf(0.0, normal.dot(photon->normal));
		l += light * normalContribution;
	}

	int dimension = photon->axis;
	float distanceToSplitPlane = position[dimension] - photon->position[dimension];

	if(fabsf(distanceToSplitPlane) <= d) {
		kdSearchRange(d, position, photon->left, normal, l);
		kdSearchRange(d, position, photon->right, normal, l);
	} else if(distanceToSplitPlane > 0) {
		kdSearchRange(d, position, photon->right, normal, l);
	} else {
		kdSearchRange(d, position, photon->left, normal, l);
	}
}

std::string PhotonMap::toString()
{
	std::string s = "";

	for(int i = 0; i < m_photons.size(); i++) {
		Photon *p = m_photons[i];
		s.append(p->toString() + "\n");
	}

	return s;
}

NORI_NAMESPACE_END
