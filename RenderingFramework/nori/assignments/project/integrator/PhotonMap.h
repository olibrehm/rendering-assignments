/*
 * PhotonMap.h
 *
 *  Created on: Jan 27, 2016
 *      Author: parallels
 */

#ifndef ASSIGNMENTS_PROJECT_INTEGRATOR_PHOTONMAP_H_
#define ASSIGNMENTS_PROJECT_INTEGRATOR_PHOTONMAP_H_

#define CONE_K 1.0

#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/scene.h>

NORI_NAMESPACE_BEGIN

struct Photon
{
	Point3f position;
	Vector3f direction;
	Vector3f normal;
	Color3f intensity;

	Photon* left;
	Photon* right;

	int axis; // splitting plane

	Photon(Point3f position, Vector3f direction, Vector3f normal, Color3f intensity)
	{
		this->position = position;
		this->direction = direction;
		this->intensity = intensity;
		this->normal = normal;

		this->left=NULL;
		this->right=NULL;
		this->axis= 0;
	}

	std::string toString()
	{
		return "Photon[position: " + position.toString().toStdString() + ", direction: " + direction.toString().toStdString() + ", " + intensity.toString().toStdString() + "]";
	}

	Color3f contributionTo(Point3f &p, float maxDist)
	{
		float distance = (position - p).norm();

		if(distance > maxDist) {
			return Color3f(0.0);
		}

		//return intensity;
		return (1.0 - distance / (CONE_K * maxDist)) * intensity;
	}

	float distanceTo( const Photon *rhs )
	{
		return (this->position - rhs->position).norm();
	}

	float distanceTo(const Point3f position)
	{
		return (this->position - position).norm();
	}
};

/* The kd-tree for the storage of photons */
class PhotonMap {
public:
	PhotonMap();
	~PhotonMap();

	void addPhoton(Point3f position, Vector3f direction, Vector3f normal, Color3f intensity);

	void clear();

	void buildKdTree();

	Color3f contributionAroundPoint(Point3f position, Vector3f normal, float maxDist);

	std::string toString();

	void writePLY(std::string path);

private:

	Photon* kdBalance(size_t start, size_t end);
	int kdFindLargestDimension(size_t start, size_t end);

	void kdSearchRange(float d, Point3f position, Photon *photon, Vector3f normal, Color3f &l);

	std::vector<Photon*> m_photons;
	bool m_kdTreeBuilt;

	Photon *m_root;
	int m_kdProgress;
};

NORI_NAMESPACE_END

#endif /* ASSIGNMENTS_PROJECT_INTEGRATOR_PHOTONMAP_H_ */
