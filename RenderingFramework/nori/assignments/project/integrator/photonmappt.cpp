/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/scene.h>

#include "PhotonMap.h"

NORI_NAMESPACE_BEGIN

class PhotonMapIntegrator : public Integrator {
public:
	PhotonMapIntegrator(const PropertyList &propList):Integrator(propList) {
		m_maxBounces = propList.getInteger("maxBounces", 64);
		m_numPhotons = propList.getInteger("numPhotons", 100000);
		m_photonRadius = propList.getFloat("photonRadius", 16.0);

		m_photonMap = new PhotonMap();
	}

	void prePass(const Scene *scene, Sampler *sampler) const
	{
		cout << "PHOTONMAP prepass..." << endl;

		m_photonMap->clear();

		for(int photonIndex = 0; photonIndex < m_numPhotons;) {
			if(photonIndex % 10000 == 0) {
				cout << "\rPrepass progress: " << (photonIndex * 100 / m_numPhotons) << "%" << std::flush;
			}

			// shoot light ray
			LuminaireQueryRecord lRec;

			Color3f intensity = scene->sampleLightRay(lRec, sampler) / m_numPhotons * lRec.pdf;

			Intersection its;
			Ray3f ray(lRec.lp, lRec.ws_wi);
			ray.mint = Epsilon;

			for(int j = 0; j < m_maxBounces && photonIndex < m_numPhotons; j++) {
				if(!scene->rayIntersect(ray, its)) {
					break;
				}

				const BSDF *bsdf = its.mesh->getBSDF();

				// shoot new ray
				BSDFQueryRecord bRec;
				bRec.wi = its.toLocal(-ray.d);
				bRec.its = its;

				intensity *= bsdf->sample(bRec, sampler);

				if(bRec.measure == ESolidAngle) {
					Vector3f normal = its.toWorld(Vector3f(0.0, 1.0, 0.0));
					normal.normalize();
					m_photonMap->addPhoton(its.p, its.toLocal(-ray.d), normal, intensity);
					photonIndex++;
				}

				ray = Ray3f(its.p, its.toWorld(bRec.wo));
			}
		}

		m_photonMap->buildKdTree();
	}


	~PhotonMapIntegrator()
	{
		delete m_photonMap;
	}

	Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
		Intersection its;

		Color3f light = Color3f(0.0);
		Color3f throughput = Color3f(1.0);

		Ray3f currentRay = ray;

		for(int i = 0; i < m_maxBounces; i++) {
			if (!scene->rayIntersect(currentRay, its)) {
				return light;
			}

			// direct light source hit
			const Luminaire *l = its.mesh->getLuminaire();
			if(l != NULL) {
				light += throughput * l->power();
			}

			BSDFQueryRecord bRec;
			bRec.wi = its.toLocal(-ray.d);
			bRec.its = its;

			throughput *= its.mesh->getBSDF()->sample(bRec, sampler);

			if(bRec.measure == ESolidAngle) {
				Vector3f normal = its.toWorld(Vector3f(0.0, 1.0, 0.0));
				normal.normalize();
				return light + throughput * m_photonMap->contributionAroundPoint(its.p, normal, m_photonRadius);
			}

			// no diffuse surface hit, shoot next ray
			currentRay = Ray3f(its.p, its.toWorld(bRec.wo));
		}

		return light;
	}

	QString toString() const {
		return QString("PhotonMapIntegrator");
	}
private:
	int m_maxBounces;
	int m_numPhotons;
	float m_photonRadius;

	PhotonMap *m_photonMap;
};

NORI_REGISTER_CLASS(PhotonMapIntegrator, "pmpath");
NORI_NAMESPACE_END

