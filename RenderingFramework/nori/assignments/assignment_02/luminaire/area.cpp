#include <nori/luminaire.h>
#include <nori/bitmap.h>

#include <nori/frame.h>

#include <nori/mesh.h>

#include <nori/sampler.h>

NORI_NAMESPACE_BEGIN

class AreaLight: public Luminaire{
public:
	AreaLight(const PropertyList &propList){
		m_radiance = propList.getColor("radiance");
	}
	~AreaLight(){
	}


	void setParent(NoriObject *obj){
		m_mesh = static_cast<Mesh*>(obj);
		m_surfaceArea = m_mesh->surfaceArea();
	}

	Color3f sampleDirect(LuminaireQueryRecord &lRec, Point2f sample) const{
		const Mesh *mesh = this->m_mesh;

		Normal3f normal_ws;
		mesh->samplePosition(sample, lRec.lp, normal_ws);

		lRec.ws_wi = lRec.sp - lRec.lp;
		lRec.distance = lRec.ws_wi.norm();
		lRec.ws_wi.normalize();
		normal_ws.normalize();

		Frame frame(normal_ws);
		lRec.wi = frame.toLocal(lRec.ws_wi);

		lRec.pdf = this->pdfDirect(lRec);

		float costheta = lRec.ws_wi.dot(normal_ws);
		if(costheta < 0) {
			return Color3f(0.0f);
		}

		return this->eval(lRec) / pdfDirect(lRec);
	}

	Color3f sampleLightRay(LuminaireQueryRecord &lRec, Sampler *sampler) const
	{
		const Mesh *mesh = this->m_mesh;

		Normal3f normal_ws;
		mesh->samplePosition(sampler->next2D(), lRec.lp, normal_ws);

		lRec.wi = squareToUniformHemisphere(sampler->next2D());

		Frame frame(normal_ws);
		lRec.ws_wi = frame.toWorld(lRec.wi);

		//cout << "ws_wi: " << lRec.ws_wi << endl;
		return this->power();
	}

	float pdfDirect(LuminaireQueryRecord &lRec)const{
		float costheta = Frame::cosTheta(lRec.wi);
		if(costheta < 0) {
			return 0.0;
		}

		return (1.0 / m_surfaceArea) / costheta *
				(lRec.distance * lRec.distance);
	}


	Color3f eval(LuminaireQueryRecord &lRec)const{
		if(Frame::cosTheta(lRec.wi)<0.0f){
			return Color3f(0.0);
		}

		return m_radiance;
	}

	inline Color3f power()const{
		return m_radiance * m_surfaceArea * M_PI;
	}

	QString toString() const {
		return QString(
			"AreaLight[\n"
			"  radiance = %1,\n"
			"]")
			.arg(m_radiance.toString());
	}

private:
	Color3f m_radiance;
	Mesh* m_mesh;
	float m_surfaceArea;
};

NORI_REGISTER_CLASS(AreaLight, "area");
NORI_NAMESPACE_END
