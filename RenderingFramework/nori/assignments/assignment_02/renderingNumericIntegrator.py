# -*- coding: utf-8 -*-
"""
Created on Fri Nov 13 10:40:11 2015

@author: Simon Poschenrieder & Oliver Brehm
"""

import random as r
import math as m
import matplotlib.pyplot as plt
import numpy as np


def func_a( x ):
    return x*x
    
def func_b( x ):
    return m.cos( x )
    
def func_c( x ):
    return m.cos( x )
    
def func_d( x ):
    return 7*x*x*x+3*x*x-2*x+10
    
def func_e( x ):
    return m.sin( x )*m.cos( x )

def func_aa( x, C ):
    return float(1)/3*x*x*x + C
    
def func_bb( x, C ):
    return m.sin( x ) + C
    
def func_cc( x, C ):
    return m.sin( x ) + C
    
def func_dd( x, C ):
    return float(7)/4*x*x*x*x+x*x*x-x*x+10*x + C
    
def func_ee( x, C ):
    return float(-1)/2*m.cos( x )*m.cos( x ) + C
 
# the function   
func = {'a' : func_a, 'b' : func_b, 'c' : func_c, 'd' : func_d, 'e' : func_e }
# the antiderivative   
antideri = {'a' : func_aa, 'b' : func_bb, 'c' : func_cc, 'd' : func_dd, 'e' : func_ee }
# the upper bound
u = {'a' : 1, 'b' : m.pi, 'c' : 2*m.pi, 'd' : 1, 'e' : 1 }
#the lower bound
l = {'a' : 0, 'b' : 0, 'c' : 0, 'd' : 0, 'e' : 0 }
#the colors for the plot
colors = {'a' : 'red', 'b' : 'blue', 'c' : 'green', 'd' : 'yellow', 'e' : 'black' }



# The different task with there
# integral bounds and functions
# can be set here.
t='e'
# The amount of samples can be set here
N=2000
solution=antideri[t](u[t], 0)-antideri[t](l[t], 0)
print( solution )
errors=[]
samplesSum=0
for i in range(1,N):
    samplesSum=samplesSum+func[t]( r.uniform( u[t], l[t] ) )
    errors.append( solution - samplesSum/i ) 


plt.plot( range(1,N), np.array(errors), c=colors[t] )
plt.show()







