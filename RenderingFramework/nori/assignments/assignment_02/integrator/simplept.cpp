/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/scene.h>

NORI_NAMESPACE_BEGIN


class SimplePTIntegrator : public Integrator {
public:
	SimplePTIntegrator(const PropertyList &propList):Integrator(propList) {
		m_maxBounces = propList.getInteger("maxBounces", 50);
		m_rrMinBounces = propList.getInteger("rrMinBounces", 10);
	}

	Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
		Ray3f currentRay = ray;
		Color3f light(0.0f);
		Color3f reflection(1.0f);

		for(int i = 0; i< m_maxBounces; i++) {
			float pRR = sampler->next1D();
			if(i >= m_rrMinBounces && pRR >= 0.5) {
				// russian roulette discard
				break;
			}

			Intersection its;
			if (!scene->rayIntersect(currentRay, its)){
				const Luminaire *luminaire = scene->getEnvMap();
				if(luminaire != NULL) {
					LuminaireQueryRecord lQuery(-currentRay.d);
					light += luminaire->eval(lQuery) * reflection;
				}
				break;
			}

			Point2f sample = sampler->next2D();

			Vector3f wi = its.toLocal(-ray.d);
			Vector3f wo = squareToUniformHemisphere(sample);

			float pdf = 1.0/(2.0*M_PI);

			const Luminaire *luminaire = its.mesh->getLuminaire();
			if(luminaire != NULL) {
				// stop if light source hit
				LuminaireQueryRecord lQuery(wo);
				light += luminaire->eval(lQuery) * reflection;
			}

			// compute bsdf term and shoot new ray
			const BSDF *bsdf = its.mesh->getBSDF();
			BSDFQueryRecord bsdfQuery(wi, wo , ESolidAngle);
			bsdfQuery.its = its;
			reflection *= (bsdf->eval(bsdfQuery) * Frame::cosTheta(wo))/pdf;

			currentRay = Ray3f(its.p, its.toWorld(wo));
		}

		// maximum of bounces reached
		return  light;
	}

	QString toString() const {
		return QString("DepthIntegrator");
	}
private:
	int m_maxBounces;
	int m_rrMinBounces;
};

NORI_REGISTER_CLASS(SimplePTIntegrator, "simplepath");
NORI_NAMESPACE_END

