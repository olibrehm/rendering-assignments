/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/bsdf.h>
#include <nori/frame.h>
#include <nori/texture.h>

NORI_NAMESPACE_BEGIN

/**
 * \brief Diffuse / Lambertian BRDF model
 */
class Mirror : public BSDF {
public:
	Mirror(const PropertyList &propList) {
	}

	/// Evaluate the BRDF model
	Color3f eval(const BSDFQueryRecord &bRec) const {
		if(bRec.measure == ESolidAngle) {
			return Color3f(0.0);
		} else {
			return Color3f(1.0);
		}
	}

	bool isMirrored(Vector3f a, Vector3f b) const {
		// check (ax, ay, ax) =EPSILON= (-bx, -by, bz)
		return (a[0] < -b[0] + EPSILON && a[0] > -b[0] - EPSILON)
				&& (a[1] < -b[1] + EPSILON && a[1] > -b[1] - EPSILON)
				&& (a[2] < b[2] + EPSILON && a[2] > b[2] - EPSILON);
	}

	/// Return a human-readable summary
	QString toString() const {
		return QString(
			"Mirror");
	}

	Color3f sample(BSDFQueryRecord &bRec, Sampler *sampler) const{
		bRec.wo = Vector3f(-bRec.wi[0], -bRec.wi[1], bRec.wi[2]);
		bRec.measure = EDiscrete;

		float pdf = this->pdf(bRec);
		Color3f fr = this->eval(bRec);

		bRec.pdf = pdf;

		return fr / pdf;
	}

	float pdf(const BSDFQueryRecord &bRec) const{
		if(bRec.measure == ESolidAngle) {
			return 0.0;
		} else {
			return 1.0;
		}
	}

	EClassType getClassType() const { return EBSDF; }
private:
	float EPSILON = 0.1;
};

NORI_REGISTER_CLASS(Mirror, "mirror");
NORI_NAMESPACE_END
