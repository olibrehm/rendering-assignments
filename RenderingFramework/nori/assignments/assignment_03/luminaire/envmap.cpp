
#include <nori/luminaire.h>
#include <nori/bitmap.h>

#include <nori/frame.h>

#include <nori/mesh.h>

#include <nori/scene.h>

#include <nori/sampler.h>

NORI_NAMESPACE_BEGIN

class EnvmapLight: public Luminaire{
public:
	EnvmapLight(const PropertyList &propList){
		m_filename = getFullFilePath(propList.getString("filename", ""));
		m_bitmap = new Bitmap(m_filename);
		m_res = Point2f(m_bitmap->cols(),m_bitmap->rows());
		m_toWorld = propList.getTransform("toWorld");

		m_power = this->samplePower();
	}
	~EnvmapLight(){
		delete m_bitmap;
	}

	void setParent(NoriObject *obj){
		Scene *scene = (Scene*) obj;
		scene->setEnvMap(this);
	}

	Color3f sampleLightRay(LuminaireQueryRecord &lRec, Sampler *sampler) const
	{
		// TODO direction?
		lRec.wi = squareToUniformSphere(sampler->next2D());
		lRec.ws_wi = lRec.wi;

		lRec.pdf = this->pdfDirect(lRec);

		return this->eval(lRec) / pdfDirect(lRec);
	}

	Color3f sampleDirect(LuminaireQueryRecord &lRec, Point2f sample) const{
		Vector3f d = squareToUniformSphere(sample);
		lRec.wi = d;

		return this->eval(lRec) / this->pdfDirect(lRec);
	}

	float pdfDirect(LuminaireQueryRecord &lRec)const{
		return 1 / (4 * M_PI); // sphere surface
	}

	Color3f eval(LuminaireQueryRecord &lRec)const{
		if(m_bitmap == NULL) {
			return Color3f(0.0f);
		}

		Vector3f d = -lRec.wi;
		d = m_toWorld.inverse()*d;

		float x = d[0];
		float y = d[1];
		float z = d[2];

		// (u,v) = ( 1 + atan2(Dx,-Dz) / pi, arccos(Dy) / pi)
		float u = (1.0f + atan2f(x, -z) / M_PI) / 2.0f;
		//(1+atan2(d.x(), -d.z())/M_PI)/2

		float v = acosf(y) / M_PI;
		// acos(d.y())/M_PI

		u = fminf(1.0f, fmaxf(0.0f, u));
		v = fminf(1.0f, fmaxf(0.0f, v));

		//float phi = M_PI * v;

		int cx = u * (m_res[0]);
		int cy = v * (m_res[1]);

		return m_bitmap->coeff(cy, cx);
	}

	inline Color3f power()const{
		return m_power;
	}

	QString toString() const {
		return QString(
			"Envmap light[\n"
			"  filename = %1,\n"
			"]")
			.arg(m_filename);
	}

private:
	QString m_filename;
	Bitmap *m_bitmap;
	Point2f m_res;
	Transform m_toWorld;
	Color3f m_power;

	static const int NUM_SAMPLES_POWER = 1000;

	Color3f samplePower()
	{
		m_power = Color3f(0.0f);

		int n = (int) sqrtf((float) NUM_SAMPLES_POWER);

		for(int i = 0; i < n; i++) {
			for(int j = 0; j < n; j++) {
				Point2f sample(1.0 / i, 1.0 / j);
				Vector3f d = squareToUniformSphere(sample);
				LuminaireQueryRecord lRec;
				lRec.wi = d;
				m_power += this->eval(lRec);
			}
		}

		m_power = m_power / (n * n) * 4 * M_PI * M_PI;
	}
};

NORI_REGISTER_CLASS(EnvmapLight, "envmap");
NORI_NAMESPACE_END

