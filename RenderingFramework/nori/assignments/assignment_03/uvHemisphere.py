# -*- coding: utf-8 -*-
"""
Created on Fri Nov 13 10:40:11 2015

@author: Simon Poschenrieder & Oliver Brehm
"""

import random as r
import math as m
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

def func( u, v ):
    return [ sqrt(1-u*u)*cos( 2*math.pi*v ), sqrt(1-u*u)*sin( 2*math.pi*v ), u ]

N_pdf=50
N=250
X=[]
Y=[]
Z=[]
X_pdf=[]
Y_pdf=[]
Z_pdf=[]
for i in range( 1, N ):
    x, y, z=func( r.random(), r.random() )
    
    X.append( x )
    Y.append( y )
    Z.append( z )
for i in range( 1, N_pdf ):
    for j in range( 1, N_pdf ):
        X_pdf.append( float(i)/N_pdf*math.pi/2 )
        Y_pdf.append( float(j)/N_pdf*2*math.pi )
        Z_pdf.append( 1.0/(math.pi*math.pi) )
        
#ax.scatter( X, Y, Z, c='#FF0000' )
ax.scatter( X_pdf, Y_pdf, Z_pdf, c='#00FF00' )

ax.set_zlim( 0, 0.2 )
ax.set_xlim( 0, math.pi/2 )
ax.set_ylim( 0, 2*math.pi )

plt.show()






