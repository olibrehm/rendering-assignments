/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/scene.h>

NORI_NAMESPACE_BEGIN


class BSDFPTIntegrator : public Integrator {
public:
	BSDFPTIntegrator(const PropertyList &propList):Integrator(propList) {
		m_maxBounces = propList.getInteger("maxBounces", 50);
		m_rrMinBounces = propList.getInteger("rrMinBounces", 10);
	}

	Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
		Ray3f currentRay = ray;
		Color3f light(0.0f);
		Color3f reflection(1.0f);

		BSDFQueryRecord currendBSDFQuery;

		for(int i = 0; i< m_maxBounces; i++) {
			// p 68 veach
			float qi = fmaxf(1.0 / (i - m_rrMinBounces + 1), 1.0);
			if(i >= m_rrMinBounces) {
				if(sampler->next1D() > qi) {
					// russian roulette discard
					break;
				}
			}

			Intersection its;
			const Luminaire *luminaire;
			if (!scene->rayIntersect(currentRay, its)){
				luminaire = scene->getEnvMap();
				if(luminaire != NULL) {
					LuminaireQueryRecord lQuery(-currentRay.d);
					light += luminaire->eval(lQuery) * reflection / qi;
				}
				break;
			} else {
				luminaire = its.mesh->getLuminaire();
			}

			const BSDF *bsdf = its.mesh->getBSDF();
			//BSDFQueryRecord bsdfQuery(wi);
			currendBSDFQuery.wi = its.toLocal(-currentRay.d);
			currendBSDFQuery.its = its;
			currendBSDFQuery.measure = ESolidAngle;

			if(luminaire != NULL) {
				// add light if source hit
				LuminaireQueryRecord lQuery(currendBSDFQuery.wi);
				// lquery position?
				light += luminaire->eval(lQuery) * reflection / qi;
			}

			Color3f sampleColor = bsdf->sample(currendBSDFQuery, sampler);

			reflection *= sampleColor;

			// shoot new ray
			currentRay = Ray3f(its.p, its.toWorld(currendBSDFQuery.wo));
		}

		// maximum of bounces reached
		return  light;
	}

	QString toString() const {
		return QString("BSDFPTIntegrator");
	}
private:
	int m_maxBounces;
	int m_rrMinBounces;
};

NORI_REGISTER_CLASS(BSDFPTIntegrator, "bsdfpath");
NORI_NAMESPACE_END

