#include <nori/bsdf.h>
#include <nori/frame.h>
#include <nori/texture.h>

NORI_NAMESPACE_BEGIN

class Dielectric : public BSDF {
public:
	Dielectric(const PropertyList &propList) {
		m_extIOR = propList.getFloat("extIOR", 1.f);
		m_intIOR = propList.getFloat("intIOR", 1.f);
	}

	/// Evaluate the BRDF model
	Color3f eval(const BSDFQueryRecord &bRec) const {
		return Color3f(0.0f);
	}

	//from Physical based Rendering, chpt 8
	Color3f sample(BSDFQueryRecord &bRec, Sampler *sampler) const {
		float reflectance;
		float cosI = std::max(-1.f, std::min(1.f, Frame::cosTheta(bRec.wi)));

		// Check if entering or going out of Medium
		float extIOR = m_extIOR;
		float intIOR = m_intIOR;
		if(cosI < 0.f)
			std::swap(extIOR, intIOR);

		// Snells Law, when angle of incoming ray is too flat, it's reflected totally
		// (depends on refraction index of course)
		float sinT = (extIOR/intIOR) * sqrtf(std::max(0.f, 1.f - cosI*cosI));
		// When sinT > 1, critical angle is reached, so ray is fully reflected
		if(sinT > 1.f) {
			reflectance = 1.f;
		}
		// Use fresnel term to calculate factor of
		else {
			float cosT = sqrtf(std::max(0.f, 1.f - sinT*sinT));
			float cosIAbs = fabsf(cosI);

			// Fresnel Term for dielectrics
			float Rparl = ((extIOR*cosIAbs) - (intIOR*cosT))/
						  ((extIOR*cosIAbs) + (intIOR*cosT));
			float Rperp =  ((intIOR*cosIAbs) - (extIOR*cosT))/
						   ((intIOR*cosIAbs) + (extIOR*cosT));
			reflectance = (Rparl*Rparl + Rperp*Rperp) / 2.f;

			// flip sinT when going to enter a medium (used for z direction of next ray)
			sinT = cosI > 0 ? -cosT : cosT;
		}

		// Set query record
		bRec.measure = EDiscrete;
		bRec.pdf = 1.0;

		// Reflect
		if(sampler->next1D() <= reflectance) {
			bRec.pdf = reflectance;
			bRec.wo = Vector3f(-bRec.wi.x(), -bRec.wi.y(), bRec.wi.z());
			//return Color3f(reflectance)/bRec.pdf;
			return Color3f(1.f);
		}
		// Transmit
		else {
			bRec.pdf = 1.f-reflectance;
			bRec.wo = Vector3f((-extIOR/intIOR)*bRec.wi.x(), (-extIOR/intIOR)*bRec.wi.y(), sinT);
			return Color3f(1.f);
		}

		return Color3f(0.0f);
	}

	float pdf(const BSDFQueryRecord &bRec) const {
		return 0.f;
	}

	/// Return a human-readable summary
	QString toString() const {
		return QString(
			"Dielectric[]");
	}


	EClassType getClassType() const { return EBSDF; }
private:
	float m_extIOR;
	float m_intIOR;
};

NORI_REGISTER_CLASS(Dielectric, "dielectric");
NORI_NAMESPACE_END
