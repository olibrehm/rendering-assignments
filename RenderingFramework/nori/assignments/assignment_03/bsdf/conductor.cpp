#include <nori/bsdf.h>
#include <nori/frame.h>
#include <nori/texture.h>

NORI_NAMESPACE_BEGIN

class Conductor : public BSDF {
public:
	Conductor(const PropertyList &propList) {
		m_eta = propList.getFloat("eta", 0.5f);
		m_k = propList.getFloat("k", 2.f);
	}

	/// Evaluate the BRDF model
	Color3f eval(const BSDFQueryRecord &bRec) const {
		return Color3f(0.0f);
	}

	Color3f sample(BSDFQueryRecord &bRec, Sampler *sampler) const {
		/*float reflectance;
		float cosI = Frame::cosTheta(bRec.wi);

		// Fresnel
		float tmp = (m_eta*m_eta + m_k*m_k) * cosI*cosI;
		float Rparl2 = (tmp - (2.f * m_eta * cosI) + 1)/
					   (tmp + (2.f * m_eta * cosI) + 1);

		float tmp_f = m_eta*m_eta + m_k*m_k;
		float Rperp2 = (tmp_f - (2.f * m_eta * cosI) + cosI*cosI) /
					   (tmp_f + (2.f * m_eta * cosI) + cosI*cosI);*/

		//reflectance = (Rparl2 + Rperp2) / 2.f;

		// Set query record
		bRec.measure = EDiscrete;
		bRec.pdf = 1.f;

		float sampler1D = sampler->next1D();

		// Reflect
		bRec.pdf = 1.f;//reflectance;
		bRec.wo = Vector3f(-bRec.wi.x(), -bRec.wi.y(), bRec.wi.z());
		return Color3f(1.f);//reflectance);
	}

	float pdf(const BSDFQueryRecord &bRec) const {
		return 0.f;
	}

	/// Return a human-readable summary
	QString toString() const {
		return QString(
			"Conductor[]");
	}


	EClassType getClassType() const { return EBSDF; }
private:
	float m_eta;
	float m_k;
};

NORI_REGISTER_CLASS(Conductor, "conductor");
NORI_NAMESPACE_END
/*
#include <nori/bsdf.h>
#include <nori/frame.h>
#include <nori/texture.h>

NORI_NAMESPACE_BEGIN


class Conductor : public BSDF {
public:
	Conductor(const PropertyList &propList) {
		m_eta = propList.getFloat("eta", 0.5);
		m_k = propList.getFloat("k", 2.5);
	}

	/// Evaluate the BRDF model
	Color3f eval(const BSDFQueryRecord &bRec) const {

		if (bRec.measure != ESolidAngle
			|| Frame::cosTheta(bRec.wi) <= 0
			|| Frame::cosTheta(bRec.wo) <= 0
			) {
			return Color3f(0.0f);
		}

		//float theta = bRec.wi.dot((Vector3f(0,0,1))) / bRec.wi.norm();
		//return Color3f(fresnel(m_eta, m_k, theta));
		return Color3f(0.5f);
	}

	Color3f sample(BSDFQueryRecord &bRec, Sampler *sampler) const{
		// sample cosine hemisphere
		//bRec.wo = squareToCosineHemisphere(sampler->next2D());
		//return (this->eval(bRec) * Frame::cosTheta(bRec.wo)) / this->pdf(bRec);
		return Color3f(0.5);
	}

	float pdf(const BSDFQueryRecord &bRec) const{
		// pdf of cosine hemisphere is cos(theta) / pi. See http://www.rorydriscoll.com/2009/01/07/better-sampling/
		return Frame::cosTheta(bRec.wo) * INV_PI;
	}

	static float fresnel(float n, float k, float theta) {
		float nk = n*n + k*k;
		float cos = cosf(theta);
		float cos2 = cos * cos;
		float ncos = 2 * n * cos;

		float r1z = nk * cos2 - ncos + 1;
		float r1n = nk * cos2 + ncos + 1;
		float r1 = r1z / r1n;

		float r2z = nk - ncos + cos2;
		float r2n = nk + ncos + cos2;
		float r2 = r2z / r2n;

		return 0.5 * (r1 * r1 + r2 * r2);
	}

	/// Return a human-readable summary
	QString toString() const {
		return QString(
			"Conductor[\n"
			"  eta = %1\n"
			"  k = %2\n"
			"]").arg(m_eta, m_k);
	}

	EClassType getClassType() const { return EBSDF; }
private:
	float m_eta;
	float m_k;
};

NORI_REGISTER_CLASS(Conductor, "conductor");
NORI_NAMESPACE_END*/
