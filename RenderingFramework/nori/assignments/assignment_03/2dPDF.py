# -*- coding: utf-8 -*-
"""
Created on Fri Nov 13 10:40:11 2015

@author: Simon Poschenrieder & Oliver Brehm
"""

import random as r
import math as m
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D


def func( x, y ):
    return cos( x )*cos( x )+sin( y )*sin( y )

def antideriFunc( x, y, Cy=0, Cx=0 ):
    return ( y*sin( 2*x )+( 4*y+4*Cy-sin( 2*y ) )*x )/4+Cx    
        
        
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

NumberOfX=30
lowX=-math.pi/2
highX=math.pi/2
NumberOfY=30
lowY=0
highY=math.pi
rangeX=np.arange( lowX, highX, (highX-lowX)/NumberOfX )
rangeY=np.arange( lowY, highY, (highY-lowY)/NumberOfY )
X=[]
Y=[]
Z=[]
antideri_Z=[]
minZ=0
maxZ=1
for x in rangeX:
    for y in rangeY:
        z=func( x, y )
        az=antideriFunc( x, y )
        if z>maxZ:
            maxZ=z
        if z<minZ:
            minZ=z
        X.append( x )
        Y.append( y )
        Z.append( z )
        antideri_Z.append( az )

ax.scatter( X, Y, Z, c='#FF0000' )
ax.scatter( X, Y, antideri_Z, c='#00FF00' )

ax.set_zlim( minZ-0.2, maxZ+0.2 )

plt.show()






