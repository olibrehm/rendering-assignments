# -*- coding: utf-8 -*-
"""
Created on Fri Nov 13 10:40:11 2015

@author: Simon Poschenrieder & Oliver Brehm
"""

import random as r
import math as m
import matplotlib.pyplot as plt
import numpy as np

numericIntegralG=[]

def func_f( x ):
    y=0
    if x>=0.4 and x<=0.6:
        y=5

    return y
    
def func_g( x ):
    sd=0.1
    mu=0.5

    y=0
    if x>=0.0 and x<=1.0:
        y=1/(sd*sqrt(2*math.pi))*e**((-(x-mu)**2)/(2*sd**2))

    return y
    
def func_h( x ):
    y=0
    if x>=0.0 and x<=1.0:
        y=(math.pi/2)*sin(x*math.pi/2)

    return y
    
def func_ff( x, C=-2 ):
    y=0
    if x>=0.4 and x<=0.6:
        y=5*x+C

    return y
    
def func_gg( x, C ):
    y=0
    g=size(numericIntegralG)
    if g==0:
        NN=10000
        # Calculate numeric integral
        w=1/(float(NN))
        numericIntegralG.append( func_g( w/2 ) )
        for i in range(1,NN-1):
            numericIntegralG.append( w*func_g( i/float(N)+w/2 ) + numericIntegralG[i-1] )
    
    g=size(numericIntegralG)
    if x>=0.0 and x<=1.0:
        y=numericIntegralG[int(x*g)]+C
    
    if x>1.0:
        y=1
        
    return y
    
def func_hh( x, C ):
    y=0
    if x>=0.0 and x<=1.0:
        y=-cos(x*math.pi/2)+C

    return y
    
# the function   
func = {'f' : func_f, 'g' : func_g, 'h' : func_h }

# the colors for the plot and accepted
colors = {'f' : '#FF0000', 'g' : '#00FF00', 'h' : '#0000FF' }

# the colors for rejected
colors2 = {'f' : '#990000', 'g' : '#009900', 'h' : '#000099' }

# max values
y_max = {'f' : 5, 'g' : 4, 'h' : 1.5708 }


# The different task with there
# integral bounds and functions
# can be set here.
t='h'
# The amount of samples can be set here
N=2000
values=[]
yAxis=[]
rejected=[]
accepted=[]
for i in range(1,N):
    x=r.random()
    y=r.random()*y_max[t]
    values.append( func[t](i/float(N)) )
    yAxis.append( i/float(N) )
    if y<=func[t](x):
        accepted.append( [x,y] )
    else:
        rejected.append( [x,y] )
    
plt.plot( np.array(yAxis), np.array(values), c='#0000FF' )
plt.scatter( np.array(accepted).T[0,:], np.array(accepted).T[1,:], c='#00FF00' )
plt.scatter( np.array(rejected).T[0,:], np.array(rejected).T[1,:], c='#FF0000' )
#plt.plot( np.array(yAxis), np.array(values), c=colors[t] )
#plt.scatter( np.array(accepted).T[0,:], np.array(accepted).T[1,:], c=colors[t] )
#plt.scatter( np.array(rejected).T[0,:], np.array(rejected).T[1,:], c=colors2[t] )
plt.show()







