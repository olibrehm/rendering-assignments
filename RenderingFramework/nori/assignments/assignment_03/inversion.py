# -*- coding: utf-8 -*-
"""
Created on Fri Nov 13 10:40:11 2015

@author: Simon Poschenrieder & Oliver Brehm
"""

import random as r
import math as m
import matplotlib.pyplot as plt
import numpy as np

numericIntegralG=[]

def func_f( x ):
    y=0
    if x>=0.4 and x<=0.6:
        y=5

    return y
    
def func_g( x ):
    sd=0.1
    mu=0.5

    y=0
    if x>=0.0 and x<=1.0:
        y=1/(sd*sqrt(2*math.pi))*e**((-(x-mu)**2)/(2*sd**2))

    return y
    
def func_h( x ):
    y=0
    if x>=0.0 and x<=1.0:
        y=(math.pi/2)*sin(x*math.pi/2)

    return y
    
def func_ff( x, C=-2 ):
    y=0
    if x>=0.4 and x<=0.6:
        y=5*x+C
    if x>=0.6:
        y=1

    return y
    
def func_gg( x, C=0 ):
    y=0
    g=size( numericIntegralG )
    if g==0:
        NN=10000
        # Calculate numeric integral
        w=1/(float(NN))
        numericIntegralG.append( func_g( w/2 ) )
        for i in range( 1, NN-1 ):
            numericIntegralG.append( w*func_g( i/float( NN )+w/2 ) + numericIntegralG[i-1] )
    
    g=size( numericIntegralG )
    if x>=0.0 and x<=1.0:
        y=numericIntegralG[int(x*g)]+C
    
    if x>1.0:
        y=1
        
    return y
    
def func_hh( x, C=1 ):
    y=0
    if x>=0.0 and x<=1.0:
        y=-cos(x*math.pi/2)+C

    return y

def inv_ff( y, C=-2 ):
    x=0
    if y>=0.0 and y<=1.0:
        x=float(y-C)/5
        
    if y>=1.0:
        x=1

    return x
    
    
def inv_gg( y ):
    x=0
    g=size(numericIntegralG)
    if g==0:
        NN=10000
        # Calculate numeric integral
        w=1/(float(NN))
        numericIntegralG.append( func_g( w/2 ) )
        for i in range(1,NN-1):
            numericIntegralG.append( w*func_g( i/float( NN )+w/2 ) + numericIntegralG[i-1] )
    
    g=size(numericIntegralG)
    
    if y>=0.0 and y<=1.0:
        for i,v in enumerate(numericIntegralG):
            if v>y:
                x=float(i)/g
                break
        
        if abs(y-numericIntegralG[i-1]) < abs( y-v ) :
            x=float(i-1)/g
        
    if x>1.0:
        y=1
        
    return x
    
def inv_hh( y, C=1 ):
    x=0
    if y>=0.0 and y<=1.0:
        x=math.acos( C-y)*2/math.pi

    return x
    
# the function   
func = {'f' : func_f, 'g' : func_g, 'h' : func_h }
func_antideri = {'f' : func_ff, 'g' : func_gg, 'h' : func_hh }
inv=inv_antideri = {'f' : inv_ff, 'g' : inv_gg, 'h' : inv_hh }

# the colors for the plot and accepted
colors = {'f' : '#FF0000', 'g' : '#00FF00', 'h' : '#0000FF' }

# the colors for rejected
colors2 = {'f' : '#990000', 'g' : '#009900', 'h' : '#000099' }

# max values
y_max = {'f' : 5, 'g' : 4, 'h' : 1.5708 }


# The different task with there
# integral bounds and functions
# can be set here.
t='h'
# The amount of samples can be set here
N=40
values=[]

yAxis=[]
samples=[]
for i in range(1,N):
    x=inv[t]( r.random() )
    y=func[t]( x )
    
    values.append( func[t]( i/float( N ) ) )
    yAxis.append( i/float(N) )
    
    samples.append( [x,y] )
    
plt.plot( np.array(yAxis), np.array(values), c='#000000' )
plt.scatter( np.array(samples).T[0,:], np.array(samples).T[1,:], c='#00FF00' )
plt.scatter( np.array(samples).T[0,:], [0]*(N-1), c='#00FF00' )

plt.xlim( [ -0.1,1.1 ] )
plt.ylim( [ -0.1,y_max[t]+0.1 ] )

plt.show()







