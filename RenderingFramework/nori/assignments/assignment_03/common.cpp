#include <nori/common.h>
#include <nori/vector.h>

namespace nori{

	/// Uniformly sample a vector on the unit hemisphere with respect to projected solid angles
	Vector3f squareToCosineHemisphere(const Point2f &sample)
	{
		float u = sample.x();
		float v = sample.y();

		float t = 2 * M_PI * v;

		return Vector3f(sqrtf(u) * cos(t), sqrtf(u) * sin(t), sqrtf(fmaxf(0.0f, 1 - u)));
	}

}
