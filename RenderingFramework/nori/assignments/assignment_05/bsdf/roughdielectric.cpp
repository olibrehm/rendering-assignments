
#include <nori/bsdf.h>
#include <nori/frame.h>
#include <nori/texture.h>
#include <cmath>
NORI_NAMESPACE_BEGIN

#define PI 3.1415926536f
#define E  2.7182818285f


class RoughDielectric : public BSDF {
public:
	RoughDielectric(const PropertyList &propList) {
		m_extIOR = propList.getFloat("extIOR", 1.f);
		m_intIOR = propList.getFloat("intIOR", 1.f);
		m_alpha = propList.getFloat("alpha", 0.1f);
	}

	double xp( double mxn ) const
	{
		if( mxn > 0 )
			return 1;
		else
			return 0;
	}
	double xp( Vector3f m, Vector3f n, Vector3f v ) const
	{
		if( ( dot( v, m ) )/( dot( v, n ) ) > 0 )
			return 1;
		else
			return 0;
	}
	double sdt( double a ) const // Shadowing - Masking Decision Term
	{
		if( a<1.6 )
			return (3.535*a+2.181*a*a)/(1+2.276*a+2.577*a*a);
		else
			return 1;
	}
	double dot( Vector3f a, Vector3f b ) const
	{
		return a.x()*b.x()+a.y()*b.y()+a.z()*b.z();
	}
	double absDot( Vector3f a, Vector3f b ) const
	{
		return std::abs( dot( a, b ) );
	}
	double mag( Vector3f a ) const
	{
		return sqrtf(a.x()*a.x()+a.y()*a.y()+a.z()*a.z());
	}
	Vector3f mirrorVector_around_( Vector3f ra, Vector3f mirror ) const
	{
		ra.normalize();
		mirror.normalize();
		double s=2*dot( ra, mirror );
		Vector3f rr=Vector3f( ra.x()-s*mirror.x(), ra.y()-s*mirror.y(), ra.z()-s*mirror.z() );
		return rr;
	}
	Vector3f halfVector( Vector3f a, Vector3f b ) const
	{
		Vector3f h = b-a;
		h.normalize();
		return h;
	}
	double  angleBetweenVectors( Vector3f a, Vector3f b ) const
	{
		double aa= mag( a );
		double bb= mag( b );
		return acos((b.x()*a.x()+b.y()*a.y()+b.z()*a.z())/(aa*bb));
	}
	double fD( double mxn, double theta_m ) const
	{
		//D(m)=Xp(m*n)/(pi*alphab^2*cos(thetam)^4) *e^((-tan(thetam)^2)/(alphab^2))
		double a2=m_alpha*m_alpha;
		double ct2=cos(theta_m)*cos(theta_m);
		double ct4=ct2*ct2;
		double tt2=tan(theta_m)*tan(theta_m);
		return xp( mxn )/( PI*a2*ct4 )*pow( E, ( -tt2 )/( a2 ) );
	}
	double fG( Vector3f n, Vector3f m, const BSDFQueryRecord &bRec ) const
	{
		// G(v,m)~Xp((v*m)/(v*n))*term2
		// term2 = if( a<1.6 ){ (3.535a+2.181a^2)/(1+2.276a+2.577a^2) } else{ 1 }
		// a:= ( m_alpha*tan( thetav ) )^−1

		double theta_v=angleBetweenVectors( bRec.wo, n );
		double a=pow( m_alpha*tan( theta_v ), -1 );
		return xp( m, n, bRec.wo )*sdt( a );
	}
	/*
	double fF( Vector3f m, const BSDFQueryRecord &bRec ) const
	{


		// Fresnel
		float tmp = (m_eta*m_eta + m_k*m_k) * cosI*cosI;
		float Rparl2 = (tmp - (2.f * m_eta * cosI) + 1)/
					   (tmp + (2.f * m_eta * cosI) + 1);

		float tmp_f = m_eta*m_eta + m_k*m_k;
		float Rperp2 = (tmp_f - (2.f * m_eta * cosI) + cosI*cosI) /
					   (tmp_f + (2.f * m_eta * cosI) + cosI*cosI);

		return (Rparl2 + Rperp2) / 2.f;
	}*/

	/// Evaluate the BRDF model
	Color3f eval(const BSDFQueryRecord &bRec) const
	{
		/*
		Vector3f m=halfVector( bRec.wi, bRec.wo );
		Vector3f n=Vector3f( 0, 0, 1 );
		double theta_m= std::acos(dot(m,n));

		double D=fD( dot( m, n ), theta_m );
		//std::cout << "D:" << D << std::endl;

		// G(v,m)~Xp((v*m)/(v*n))*term2
		// term2 = if( a<1.6 ){ (3.535a+2.181a^2)/(1+2.276a+2.577a^2) } else{ 1 }
		// a:= ( m_alpha*tan( thetav ) )^−1
		double G=fG( n, m, bRec );
		//std::cout << "G:" << G << std::endl;

		double F=fF( m, bRec );

		return Color3f( ( F*G*D )/( 4*absDot( bRec.wi, m )*absDot( bRec.wo, m ) ) );
		*/
		return Color3f(1.0f);
	}

	//from Physical based Rendering, chpt 8
	Color3f sample(BSDFQueryRecord &bRec, Sampler *sampler) const {
		float reflectance;

		double theta_m=atan( sqrt( -( m_alpha*m_alpha )*log( 1-sampler->next1D() ) ) );
		double phi_m=2*PI*sampler->next1D();

		Vector3f m=Vector3f( (float)sin( theta_m )*cos( phi_m ),
							 (float)sin( theta_m )*sin( phi_m ),
							 (float)cos( theta_m ) );
		m.normalize();
		Vector3f n=Vector3f( 0, 0, 1 );

		//float cosI = std::max(-1.f, std::min(1.f, Frame::cosTheta(bRec.wi)));
		float cosToM=(float)1.5708-angleBetweenVectors( bRec.wi, m );
		float cosI = std::max(-1.f, std::min(1.f, cosToM ));

		// Check if entering or going out of Medium
		float extIOR = m_extIOR;
		float intIOR = m_intIOR;
		if(cosI < 0.f)
			std::swap(extIOR, intIOR);

		// Snells Law, when angle of incoming ray is too flat, it's reflected totally
		// (depends on refraction index of course)
		float sinT = (extIOR/intIOR) * sqrtf(std::max(0.f, 1.f - cosI*cosI));
		// When sinT > 1, critical angle is reached, so ray is fully reflected
		if(sinT > 1.f) {
			reflectance = 1.f;
		}
		// Use fresnel term to calculate factor of
		else {
			float cosT = sqrtf(std::max(0.f, 1.f - sinT*sinT));
			float cosIAbs = fabsf(cosI);

			// Fresnel Term for dielectrics
			float Rparl = ((extIOR*cosIAbs) - (intIOR*cosT))/
						  ((extIOR*cosIAbs) + (intIOR*cosT));
			float Rperp =  ((intIOR*cosIAbs) - (extIOR*cosT))/
						   ((intIOR*cosIAbs) + (extIOR*cosT));
			reflectance = (Rparl*Rparl + Rperp*Rperp) / 2.f;

			// flip sinT when going to enter a medium (used for z direction of next ray)
			sinT = cosI > 0 ? -cosT : cosT;
		}

		// Set query record
		bRec.measure = EDiscrete;
		bRec.pdf = 1.0;

		// Reflect
		if(sampler->next1D() <= reflectance)
		{
			bRec.pdf = reflectance;
			bRec.wo=mirrorVector_around_( bRec.wi, m );
			//bRec.wo = Vector3f(-bRec.wi.x(), -bRec.wi.y(), bRec.wi.z());
			//return Color3f(reflectance)/bRec.pdf;
			return Color3f(1.f);
		}
		// Transmit
		else
		{
			bRec.pdf = 1.f-reflectance;
			bRec.wo=mirrorVector_around_( bRec.wi, m );
			bRec.wo = Vector3f((extIOR/intIOR)*bRec.wo.x(), (extIOR/intIOR)*bRec.wo.y(), sinT);
			return Color3f(1.f);
		}

		return Color3f(0.0f);
	}

	float pdf(const BSDFQueryRecord &bRec) const {
		return 0.f;
	}

	/// Return a human-readable summary
	QString toString() const {
		return QString(
			"Rough Dielectric[]");
	}


	EClassType getClassType() const { return EBSDF; }
private:
	float m_extIOR;
	float m_intIOR;
	float m_alpha;
};

NORI_REGISTER_CLASS(RoughDielectric, "roughdielectric");
NORI_NAMESPACE_END





























	// Generally:
	// fs := BSDF
	// m  := micro normal
	// i  := input ray
	// o  := ouput ray
	// fs(i,o,m) = fr(i,o,m)+ft(i,o,m)

	// fr     := reflexion
	// n      := macro normal
	// hr     := Half-direction for reflection
	// F(...) := Fresnel term
	// G(...) := Bidirectional shadowing-masking function
	// D(...) := Microfacet distribution function
	// fr(i,o,n)=(F(i,hr)*G(i,o,hr)*D(hr))/(4*|i*n|*|o*n|)

	// ft     := refraction (transmission)
	// ht     := Half-direction for transmission
	// nao    := Index of refraction of media on the transmitted side
	// nai    := Index of refraction of the media on the incident side
	// ft(i,o,n)=(|i*ht|*|o*ht|)/(|i*n|*|o*n|)*(nao^2*(1-f(i,ht))G(i,o,ht)*D(ht))/(nai(i*ht)+nao*(o*ht))^2

	// alphab := wide parameter
	// thetam := angle between m & n
	// pi := 3.1415...
	// Xp := function that checks if the input is bigger than 0 -> return 1 else 0
	// D(m)=Xp(m*n)/(pi*alphab^2*cos(thetam)^4) *e^((-tan(thetam)^2)/(alphab^2))

	// G(v,m)~Xp((v*m)/(v*n))*term2
	// term2 = if( a<1.6 ){ (3.535a+2.181a^2)/(1+2.276a+2.577a^2) } else{ 1 }
	// a:= ( m_alpha*tan( thetav ) )^−1

/*
	Color3f sample( BSDFQueryRecord &bRec, Sampler *sampler ) const
	{
		// Set query record
		bRec.measure = EDiscrete;

		double theta_m=atan( sqrt( -( m_alpha*m_alpha )*log( 1-sampler->next1D() ) ) );
		double phi_m=2*PI*sampler->next1D();

		Vector3f m=Vector3f( (float)sin( theta_m )*cos( phi_m ),
							 (float)sin( theta_m )*sin( phi_m ),
							 (float)cos( theta_m ) );
		m.normalize();
		Vector3f n=Vector3f( 0, 0, 1 );
		bRec.wo=mirrorVector_around_( bRec.wi, m );

		Color3f fr=this->eval( bRec );

		double D=fD( dot( m, n ), theta_m );
		bRec.pdf=D*std::abs( dot( m, n ) );

		float cosO=1.5708-angleBetweenVectors( bRec.wi, m );
		return ( fr*cos( cosO ) )/bRec.pdf;
	}


	Color3f sample( BSDFQueryRecord &bRec, Sampler *sampler ) const
	{
		// Set query record
		bRec.measure = EDiscrete;

		double theta_m=atan( sqrt( -( m_alpha*m_alpha )*log( 1-sampler->next1D() ) ) );
		double phi_m=2*PI*sampler->next1D();

		// new normal
		Vector3f m=Vector3f( (float)sin( theta_m )*cos( phi_m ),
							 (float)sin( theta_m )*sin( phi_m ),
							 (float)cos( theta_m ) );
		Vector3f n=Vector3f( 0, 0, 1 );

		bRec.wo=mirrorVector_around_( bRec.wi, m );

		eval( bRec );
		//D(m)=Xp(m*n)/(pi*alphab^2*cos(thetam)^4) *e^((-tan(thetam)^2)/(alphab^2))
		double D=fD( dot( m, n ), theta_m );
		//std::cout << "D:" << D << std::endl;
		//bRec.pdf=D*std::abs( dot( m, n ) );

		// G(v,m)~Xp((v*m)/(v*n))*term2
		// term2 = if( a<1.6 ){ (3.535a+2.181a^2)/(1+2.276a+2.577a^2) } else{ 1 }
		// a:= ( m_alpha*tan( thetav ) )^−1
		double G=fG( n, m, bRec );
		//std::cout << "G:" << G << std::endl;

		double F=fF( m, bRec );
		//std::cout << "F:" << F << std::endl;

		// (F(i,m)*G(i,o,m)*D(m))/(4*|i*m|*|o*m|)
		double ret=( ( F*G*D )/( 4*absDot( bRec.wi, m )*absDot( bRec.wo, m ) ) )/bRec.pdf;

		double weight=(absDot( bRec.wi, m )*G)/(absDot( bRec.wi, n )*absDot( m, n ));
		ret*= weight;
		return Color3f( ret );
		//return Color3f( 0.8f );

*/
