"""
Created on Tue Jan 23 2016

@author: Simon Poschenrieder & Oliver Brehm
"""

import radicalinverse as ri
import sys
import matplotlib.pyplot as plt

def Halton(dim1, dim2, n):
    xs = []
    ys = []
    
    base1 = ri.PrimeNumber(dim1)
    base2 = ri.PrimeNumber(dim2)

    for i in range(1, n):
        xs.append(ri.RadicalInverse(base1, i))
        ys.append(ri.RadicalInverse(base2, i))

    return (xs, ys)


try:
    N=int(sys.argv[1])
    dim1=int(sys.argv[2])
    dim2=int(sys.argv[3])
except:
    sys.exit("syntax: halton N dim1 dim2")

print("N   = " + str(N))
print("dim1 = " + str(dim1))
print("dim2 = " + str(dim2))

(xValues, yValues) = Halton(dim1 ,dim2, N)
#(xValues, yValues) = Halton(7 ,8, 64)

plt.scatter(xValues, yValues, c='#0000FF')
plt.show()