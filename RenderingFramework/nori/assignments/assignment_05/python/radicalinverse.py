"""
Created on Tue Jan 23 2016

@author: Simon Poschenrieder & Oliver Brehm
"""

import math as m

def IsPrime(n):
    for i in range(2, n-1):
        if n % i == 0:
            return False

    return True

primeLimit = 99999999999
def NextPrime(n):
    i = n + 1
    while i < primeLimit:
        if IsPrime(i):
            return i

        i = i + 1

    return 0

primeNumbers=[1]
def PrimeNumber(n):
    if n < len(primeNumbers):
        return primeNumbers[i]

    p = primeNumbers[len(primeNumbers) - 1]
    while n >= len(primeNumbers):
        p = NextPrime(p)
        primeNumbers.append(p)

    return p

def RadicalInverse(base, i):
    radical = digit = 1.0 / base
    inverse = 0.0

    while i > 0:
        inverse += digit * (i % base)
        digit *= radical
        i = m.floor(i / base)

    return inverse