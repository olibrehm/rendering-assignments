"""
Created on Tue Jan 23 2016

@author: Simon Poschenrieder & Oliver Brehm
"""

import radicalinverse as ri
import sys
import matplotlib.pyplot as plt

def Hammersley(n):
    n = n * n

    xs = []
    ys = []

    for i in range(0, n - 1):
        xs.append(float(i) / n)
        ys.append(ri.RadicalInverse(2, i))

    return (xs, ys)

try:
    N=int(sys.argv[1])
except:
    sys.exit("syntax: hammersley N ")

print("N   = " + str(N))

(xValues, yValues) = Hammersley(N)

plt.scatter(xValues, yValues, c='#0000FF')
plt.show()