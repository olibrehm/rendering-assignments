/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/block.h>
#include <nori/bitmap.h>
#include <nori/rfilter.h>
#include <nori/scene.h>
#include <nori/camera.h>
#include <nori/sampler.h>
#include <nori/integrator.h>
//#include <nori/bbox.h>

#include <nori/scene.h>

NORI_NAMESPACE_BEGIN



BlockGenerator::BlockGenerator(const Vector2i &size, int blockSize, int iterations)
		: m_size(size), m_blockSize(blockSize) {
	m_numBlocks = Vector2i(
		(int) std::ceil(size.x() / (float) blockSize),
		(int) std::ceil(size.y() / (float) blockSize));
	m_blocksPerIteration = m_numBlocks.x() * m_numBlocks.y();
	m_blocksLeft = m_blocksPerIteration*iterations;
	m_direction = ERight;
	m_block = Point2i(m_numBlocks / 2);
	m_stepsLeft = 1;
	m_numSteps = 1;
	m_numIterations = iterations;
	m_iteration = 0;
	//m_timer.start();
}

bool BlockGenerator::next(ImageBlock &block) {
	m_mutex.lock();

	if (m_blocksLeft == 0) {
		m_mutex.unlock();
		return false;
	}
	
	//cout << "Blocks Left"<< m_blocksLeft<< "\t"<< m_blocksLeft % m_blocksPerIteration << endl;


	//set current block
	Point2i pos = m_block * m_blockSize;
	block.setOffset(pos);
	block.setSize((m_size - pos).cwiseMin(Vector2i::Constant(m_blockSize)));

	if (--m_blocksLeft == 0) {
		//cout << "Rendering finished (took " << m_timer.elapsed() << " ms)" << endl;
		m_mutex.unlock();
		return true;
	}

	if(m_blocksLeft % m_blocksPerIteration == 0){
		m_direction = ERight;
		m_block = Point2i(m_numBlocks / 2);
		m_stepsLeft = 1;
		m_numSteps = 1;
		m_mutex.unlock();
		return true;
	}

	//search for the next block

	do {
		switch (m_direction) {
			case ERight: ++m_block.x(); break;
			case EDown:  ++m_block.y(); break;
			case ELeft:  --m_block.x(); break;
			case EUp:    --m_block.y(); break;
		}

		if (--m_stepsLeft == 0) {
			m_direction = (m_direction + 1) % 4;
			if (m_direction == ELeft || m_direction == ERight) 
				++m_numSteps;
			m_stepsLeft = m_numSteps;
		}
	} while ((m_block.array() < 0).any() ||
	         (m_block.array() >= m_numBlocks.array()).any());

	//cout << "t "<<endl;
	m_mutex.unlock();
	return true;
}

BlockRenderThread::BlockRenderThread(const Scene *scene, Sampler *sampler, 
		BlockGenerator *blockGenerator, ImageBlock *output, int numSamples) 
	 : m_scene(scene), m_blockGenerator(blockGenerator), m_output(output) {
	m_numSamples = numSamples;
	/* Create a new sample generator for the current thread */
	m_sampler = sampler->clone();
}

BlockRenderThread::~BlockRenderThread() {
	delete m_sampler;
}

void BlockRenderThread::run() {
	try {
		const Integrator *integrator = m_scene->getIntegrator();
		const Camera *camera = m_scene->getCamera();
	
		/* Allocate a small image block local to this thread
		   that will be used to accumulate radiance samples */
		ImageBlock block(Vector2i(NORI_BLOCK_SIZE), 
			camera->getReconstructionFilter());
	
		/* Fetch a block to be rendered from the block generator */
		while (m_blockGenerator->next(block)) {
			Point2i offset = block.getOffset();
			Vector2i size  = block.getSize();
	
			/* Clear its contents */
			block.clear();
	
			/* For each pixel and pixel sample sample */
			for (int y=0; y<size.y(); ++y) {
				for (int x=0; x<size.x(); ++x) {
					m_sampler->generate(Point2i(x+offset.x(),y+offset.y()));
					//for (uint32_t i=0; i<m_sampler->getSampleCount(); ++i) {
					for (int  i=0; i<m_numSamples; ++i) {
						Point2f pixelSample = Point2f(x + offset.x(), y + offset.y()) + m_sampler->next2D();
						Point2f apertureSample = m_sampler->next2D();
	
						/* Sample a ray from the camera */
						Ray3f ray;
						Color3f value = camera->sampleRay(ray, pixelSample, apertureSample);

						/* Compute the incident radiance */
				
						value *= integrator->Li(m_scene, m_sampler, ray);
					
	
						/* Store in the image block */
						block.put(pixelSample, value);
						//block.putNoFilter(pixelSample, value);
					}
				}
			}
	
			/* The image block has been processed. Now add it to the "big"
			   block that represents the entire image */
			m_output->put(block);
		}
	} catch (const NoriException &ex) {
		cerr << "Caught a critical exception within a rendering thread: " << qPrintable(ex.getReason()) << endl;
		exit(-1);
	}
}

void BlockRenderManager::setup(Scene *scene, Sampler *sampler, ImageBlock *result, int nCores ){
	m_scene = scene;
	m_sampler = sampler;
	m_result = result;
	m_nCores = nCores;
}

void BlockRenderManager::manage(){
	
	const Camera *camera = m_scene->getCamera();
	Vector2i outputSize = camera->getOutputSize();
	
	const Integrator* integrator = m_scene->getIntegrator();
	
	const Sampler * sampler = m_scene->getSampler();

	int iterations= 1;
	int numSamples = sampler->getSampleCount();
	//if(integrator->isProgressive()){
	//	iterations = sampler->getSampleCount();
	//	numSamples = 1;
	//}
	
	BlockGenerator blockGenerator(outputSize, NORI_BLOCK_SIZE, iterations);
	
	std::vector<BlockRenderThread *> threads;
	for (int i=0; i<m_nCores; ++i) {
		BlockRenderThread *thread = new BlockRenderThread(
			m_scene, m_sampler, &blockGenerator, m_result,numSamples);
		thread->start();
		threads.push_back(thread);
	}

	/* Wait for them to finish */
	for (int i=0; i<m_nCores; ++i) {
		threads[i]->wait();
		delete threads[i];
	}

}

QString BlockRenderManager::toString() const {
	return QString(
		"Block[\n"
		"]"
		);
}

NORI_REGISTER_CLASS(BlockRenderManager, "block");
NORI_NAMESPACE_END
