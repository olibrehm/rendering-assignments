#include <nori/core.h>
#include <nori/proplist.h>

NORI_NAMESPACE_BEGIN


NoriObject* KDTree_create(const PropertyList &list);
NoriObject* PerspectiveCamera_create(const PropertyList &list);
NoriObject* BlockRenderManager_create(const PropertyList &list);
NoriObject* ProgressiveRenderManager_create(const PropertyList &list);
NoriObject* IterativeRenderManager_create(const PropertyList &list);

NoriObject* GaussianFilter_create(const PropertyList &list);
NoriObject* MitchellNetravaliFilter_create(const PropertyList &list);
NoriObject* TentFilter_create(const PropertyList &list);
NoriObject* BoxFilter_create(const PropertyList &list);

NoriObject* Independent_create(const PropertyList &list);
NoriObject* DeterministicIndependent_create(const PropertyList &list);

//NoriObject* StudentsTTest_create(const PropertyList &list);
//NoriObject* ChiSquareTest_create(const PropertyList &list);

NoriObject* WavefrontOBJ_create(const PropertyList &list);


NoriObject* SubScene_create(const PropertyList &list);

NoriObject* Scene_create(const PropertyList &list);

//NoriObject* CheckerBoardTexture2D_create(const PropertyList &list);
NoriObject* BitMapTexture2D_create(const PropertyList &list);

void Core::init(){

	//acceleration
	NoriObjectFactory::registerClass("kdtree", KDTree_create);
	//camera
	NoriObjectFactory::registerClass("perspective", PerspectiveCamera_create);
	//block
	NoriObjectFactory::registerClass("block", BlockRenderManager_create);
	//rendermanager
	NoriObjectFactory::registerClass("progressive", ProgressiveRenderManager_create);
	NoriObjectFactory::registerClass("iterative", IterativeRenderManager_create);
	//reconstructionfilter
	NoriObjectFactory::registerClass("gaussain", GaussianFilter_create);
	NoriObjectFactory::registerClass("mitchell", MitchellNetravaliFilter_create);
	NoriObjectFactory::registerClass("tent", TentFilter_create);
	NoriObjectFactory::registerClass("box", BoxFilter_create);

	//sampler
	NoriObjectFactory::registerClass("independent", Independent_create);
	NoriObjectFactory::registerClass("detindependent", DeterministicIndependent_create);
	//test
	//NoriObjectFactory::registerClass("ttest", StudentsTTest_create);
	//NoriObjectFactory::registerClass("chi2test", ChiSquareTest_create);
	
	//loader
	NoriObjectFactory::registerClass("obj", WavefrontOBJ_create);

	//subscene
	NoriObjectFactory::registerClass("xml", SubScene_create);

	NoriObjectFactory::registerClass("scene", Scene_create);
	
	//textures
	//NoriObjectFactory::registerClass("checkerboard", CheckerBoardTexture2D_create);
	NoriObjectFactory::registerClass("bitmaptexture", BitMapTexture2D_create);

}

	
NORI_NAMESPACE_END
