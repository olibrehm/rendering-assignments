#include <nori/rendermanager.h>
#include <nori/block.h>
#include <nori/integrator.h>
#include <nori/bitmap.h>

#include <nori/scene.h>

#include <nori/camera.h>

NORI_NAMESPACE_BEGIN

ProgressiveRenderManager::ProgressiveRenderManager(const PropertyList &propList) {

		m_filename = propList.getString("filename", "progressive");
		m_store = propList.getInteger("store",-1);

		if(m_store > -1){
			m_nextStore = 1;
		}else{
			m_nextStore = -1;
		}
};

void ProgressiveRenderManager::setup(Scene *scene, Sampler *sampler, ImageBlock *result, int nCores ){
	m_scene = scene;
	m_sampler = sampler;
	m_result = result;
	m_nCores = nCores;
}

void ProgressiveRenderManager::manage(){
	
	const Camera *camera = m_scene->getCamera();
	Vector2i outputSize = camera->getOutputSize();
	
	const Integrator* integrator = m_scene->getIntegrator();
	
	Sampler * sampler = m_scene->getSampler();

	integrator->prePass(m_scene, sampler);

	//int iterations= 1;
	//int numSamples = sampler->getSampleCount();
	//if(integrator->isProgressive()){
	int	iterations = sampler->getSampleCount();
	int	numSamples = 1;
	//}
	
	int k = 0;
	while(k!= iterations && !m_stop){

		BlockGenerator blockGenerator(outputSize, NORI_BLOCK_SIZE, 1);

		std::vector<BlockRenderThread *> threads;
		for (int i=0; i<m_nCores; ++i) {
			BlockRenderThread *thread = new BlockRenderThread(
				m_scene, m_sampler, &blockGenerator, m_result,numSamples);
			thread->start();
			threads.push_back(thread);
		}
	
		/* Wait for them to finish */
		for (int i=0; i<m_nCores; ++i) {
			threads[i]->wait();
			delete threads[i];
		}
		k++;

		if(k==m_nextStore){

			Bitmap *bitmap = m_result->toBitmap();

			bitmap->save(m_filename+"_"+QString::number(m_nextStore)+".exr");
			delete bitmap;
			m_nextStore*=2;
		}

	}

	cout << "Rendering finished (after " << k << " iterations)" << endl;


}

QString ProgressiveRenderManager::toString() const {
	return QString(
		"Progressive[\n"
		"]"
		);
}


void IterativeRenderManager::setup(Scene *scene, Sampler *sampler, ImageBlock *result, int nCores ){
	m_scene = scene;
	m_sampler = sampler;
	m_result = result;
	m_nCores = nCores;
}

QString IterativeRenderManager::generateOutputName(QString sceneName ,int iteration){

	QString name;
	QString snum;

	if(iteration<10){
		snum = QString("000")+QString::number(iteration);
	}else if (iteration<100){
		snum = QString("00")+QString::number(iteration);
	}else if (iteration<1000){
		snum = QString("0")+QString::number(iteration);
	}else{
		snum = QString::number(iteration);
	}

	name = sceneName + QString("_")+ snum + QString(".exr");

	return name;
}


void IterativeRenderManager::manage(){
	
	const Camera *camera = m_scene->getCamera();
	Vector2i outputSize = camera->getOutputSize();
	
	const Integrator* integrator = m_scene->getIntegrator();
	
	Sampler * sampler = m_scene->getSampler();

	integrator->prePass(m_scene, sampler);

	int	iterations = sampler->getSampleCount();
	int	numSamples = 1;

	
	
	ImageBlock img(m_result->getSize(), camera->getReconstructionFilter());

	std::vector<BlockRenderThread *> threads;
	for (int j = 0; j< iterations; j++){

		BlockGenerator blockGenerator(outputSize, NORI_BLOCK_SIZE, 1);

		img.clear();

		for (int i=0; i<m_nCores; ++i) {
			BlockRenderThread *thread = new BlockRenderThread(
				m_scene, m_sampler, &blockGenerator, &img,numSamples);
			thread->start();
			threads.push_back(thread);
		}

		// Wait for them to finish
		for (int i=0; i<m_nCores; ++i) {
			threads[i]->wait();
			delete threads[i];
		}

		threads.clear();

		QString outputname = generateOutputName("test",j);

		Bitmap *bitmap = img.toBitmap();
		//bitmap->save(outputname);
		delete bitmap;

		m_result->put(img);

		if(m_stop){
			break;
		}

	}
	/**/
}


QString IterativeRenderManager::toString() const {
	return QString(
		"Iterative[\n"
		"]"
		);
}

NORI_REGISTER_CLASS(ProgressiveRenderManager, "progressive");
NORI_REGISTER_CLASS(IterativeRenderManager, "iterative");
NORI_NAMESPACE_END
