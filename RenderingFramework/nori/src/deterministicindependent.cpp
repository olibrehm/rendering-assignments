/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/sampler.h>
#include <nori/random.h>

NORI_NAMESPACE_BEGIN

/**
 * Independent sampling - returns independent uniformly distributed
 * random numbers on <tt>[0, 1)x[0, 1)</tt>.
 *
 * This class is essentially just a wrapper around the \ref Random
 * class. For more details on what sample generators do in general,
 * refer to the \ref Sampler class.
 */
class DeterministicIndependent : public Sampler {
public:
	DeterministicIndependent(const PropertyList &propList) {
		m_sampleCount = (size_t) propList.getInteger("sampleCount", 1);
		int imgwidth = propList.getInteger("imgwidth",1);
		int imgheight = propList.getInteger("imgheight",1);
		m_imgRes = Point2i(imgwidth,imgheight);
		m_random = new Random();
	}

	virtual ~DeterministicIndependent() {
		delete m_random;
	}

	Sampler *clone() {
		DeterministicIndependent *cloned = new DeterministicIndependent();
		cloned->m_sampleCount = m_sampleCount;
		cloned->m_random = new Random();
		cloned->m_imgRes = m_imgRes;
		//cloned->generate(m_pix);
		cloned->m_random->seed(m_random);



		return cloned;
	}

	void generate(const Point2i &pixel) {
		m_pix = pixel;
		m_random->seed(m_pix[1]*m_imgRes[0]+m_pix[0]);


	}
	void advance()  { /* No-op for this sampler */ }

	float next1D() {
		return m_random->nextFloat();
	}
	
	Point2f next2D() {
		return Point2f(
			m_random->nextFloat(),
			m_random->nextFloat()
		);
	}

	QString toString() const {
		return QString("DeterministicIndependent[sampleCount=%1]").arg(m_sampleCount);
	}
protected:
	DeterministicIndependent() { }
protected:
	Random *m_random;

	Point2i m_pix;
	Point2i m_imgRes;
};

NORI_REGISTER_CLASS(DeterministicIndependent, "detindependent");
NORI_NAMESPACE_END
