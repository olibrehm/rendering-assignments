/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/parser.h>
#include <nori/scene.h>
#include <nori/camera.h>
#include <nori/block.h>
#include <nori/bitmap.h>
#include <nori/integrator.h>
#include <nori/gui.h>

#include <nori/rendermanager.h>

#include <boost/scoped_ptr.hpp>
#include <QFileInfo>
#include <QDir>
#include <QApplication>
	
#include <nori/core.h>

#include "boost/program_options.hpp"

using namespace nori;


struct NoriComandLineParameter{

	bool showGui;
	bool close;
	std::string exrFile;
	std::string sceneFile;

};


NoriComandLineParameter parseCommandLine(int argc, char **argv){

	NoriComandLineParameter ncmd;

	std::string lsrFile = "";

	namespace po = boost::program_options;
	po::options_description desc("Options");
	desc.add_options()
	  ("help", "Print help messages")
	  ("nogui","disables the gui window of nori")
	  ("close,c","closes the render window after rendering is finished")
	  ("scenefile,s",po::value<std::string>(), "scenefile")
	  ("exrfile,o",po::value<std::string>(), "exrfile");

    po::positional_options_description positionalOptions;
//    positionalOptions.add("scenefile", -1);


	po::variables_map vm;

	po::store(po::command_line_parser(argc, argv).options(desc)
			.positional(positionalOptions).run(),
			vm); // can throw

	po::notify(vm);

    /** --help option
     */
    if ( vm.count("help")  )
    {
      std::cout << "Basic Command Line Parameter App" << std::endl
                << desc << std::endl;
      return ncmd;
    }


    if(vm.count("nogui")){

    	ncmd.showGui = false;

    }else{
    	ncmd.showGui = true;
    }

    if(ncmd.showGui && vm.count("close")){

		ncmd.close = true;

	}else{
		ncmd.close = false;
	}


    if(vm.count("scenefile")){
    	ncmd.sceneFile = vm["scenefile"].as<std::string>();
    }else{
    	ncmd.sceneFile = "";
    }


    if(vm.count("exrfile")){
    	ncmd.exrFile = vm["exrfile"].as<std::string>();
    }else{
    	ncmd.exrFile = "";
    }

    return ncmd;
}


void render(Scene *scene, const QString &filename, const QString &outputName, bool showWindow = true, bool closeWindow = false) {
	const Camera *camera = scene->getCamera();
	Vector2i outputSize = camera->getOutputSize();

	/* Create a block generator (i.e. a work scheduler) */
//	BlockGenerator blockGenerator(outputSize, NORI_BLOCK_SIZE, iterations);

	/* Allocate memory for the entire output image */
	ImageBlock result(outputSize, camera->getReconstructionFilter());
	result.clear();

	NoriWindow *window = NULL;

	/* Launch the GUI */
	if(showWindow){
		window = new NoriWindow(&result);
	}


	/* Launch one render thread per core */


	int nCores = getCoreCount();

	if(scene->getMaxCores()>0){
		nCores = std::min(nCores,scene->getMaxCores());
	}

	//nCores = 1;

	RenderManager* renderManager = scene->getRenderManager();
	renderManager->setup(scene,scene->getSampler(), &result, nCores);

	renderManager->setWindow(window,closeWindow);
	renderManager->start();

	window->setRenderManager(renderManager);

	if(showWindow){
		window->setRenderManager(renderManager);
		window->startRefresh();
		qApp->exec();
		window->stopRefresh();
	}

	//wait until the render manager is finished
	renderManager->wait();

	/* Now turn the rendered image block into 
	   a properly normalized bitmap */
	Bitmap *bitmap = result.toBitmap();

	/* Determine the filename of the output bitmap */
/*
	QFileInfo inputInfo(filename);
	QString outputName = inputInfo.path() 
		+ QDir::separator() 
		+ inputInfo.completeBaseName() + ".exr";
*/
	/* Save using the OpenEXR format */
	bitmap->save(outputName);

	delete bitmap;
	delete window;
}

int main(int argc, char **argv) {
	QApplication app(argc, argv);
	Q_INIT_RESOURCE(resources);

	Core::init();


	QString sceneFile = "";
	QString outputFile = "";
	//bool showGui = true;
	
	NoriComandLineParameter ncmd= parseCommandLine( argc, argv);

	if(ncmd.sceneFile.compare("")){
		sceneFile = QString::fromStdString(ncmd.sceneFile);
		if(!ncmd.exrFile.compare("")){
			QFileInfo inputInfo(sceneFile);
			outputFile = inputInfo.path()
				+ QDir::separator()
				+ inputInfo.completeBaseName() + ".exr";
		}else{
			outputFile = QString::fromStdString(ncmd.exrFile);
		}
	}
	try{
		boost::scoped_ptr<NoriObject> root(loadScene(sceneFile));

		if (root->getClassType() == NoriObject::EScene) {
			/* The root object is a scene! Start rendering it.. */
			render(static_cast<Scene *>(root.get()), sceneFile, outputFile, ncmd.showGui, ncmd.close);
		}
	} catch (const NoriException &ex) {
		cerr << "Caught a critical exception: " << qPrintable(ex.getReason()) << endl;
		return -1;
	}

	return 0;
}
