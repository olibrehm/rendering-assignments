#include <nori/texture.h>
#include <nori/bitmap.h>


NORI_NAMESPACE_BEGIN

class BitMapTexture2D: public Texture2D{

public:
	BitMapTexture2D(const PropertyList &propList):Texture2D(propList){
		m_filename = propList.getString("filename", "");

		m_scale = propList.getColor("scale", Color3f(1.0));

		m_filename = getFullFilePath(m_filename);



		m_bitmap = new Bitmap(m_filename);
		m_res = Point2f(m_bitmap->cols(),m_bitmap->rows());
	
	
	}


	inline float modulo(float a, float b)const {
		float r = std::fmod(a, b);
		return (r < 0) ? r+b : r;
	}

	Color3f eval(const Intersection &its ,bool filter = true)const{
		Point2f uv = getFinalUV(its.uv);

		int x = modulo(uv[0]*m_res[0],m_res[0]);
		int y = modulo(uv[1]*m_res[1],m_res[1]);

		//flip y coordinate because uv starts at bottum left
		//and xy starts at top left
		y = m_res[1]-(y+1);

		//std::cout<< "m_res[0]: "<<m_res[0] <<"\tm_res[1]"<<m_res[1]<<"\tx: "<< x <<"\ty: "<<y<<std::endl;
		//std::cout << "cols(): "<<m_bitmap->cols()<<"\trows(): "<<m_bitmap->rows()<<std::endl;
		
		//remember xy is flipped in bitmap representation
		return m_bitmap->coeff(y,x)*m_scale;

		//return Color3f(uv[0],uv[1],0);
	}

	QString toString()const {
		return QString(
				"BitMapTexture[\n"
				"  filename = %1,\n"
				"]")
				.arg(m_filename);
	}

private:
	QString m_filename;
	Bitmap *m_bitmap;
	Point2f m_res;
	Color3f m_scale;

};

NORI_REGISTER_CLASS(BitMapTexture2D, "bitmaptexture");

NORI_NAMESPACE_END
