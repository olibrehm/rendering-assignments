/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2012 by Wenzel Jakob and Steve Marschner.

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/scene.h>
#include <nori/bitmap.h>
#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/camera.h>
#include <nori/medium.h>


NORI_NAMESPACE_BEGIN

Scene::Scene(const PropertyList &propList)
	: m_integrator(NULL), m_sampler(NULL), m_camera(NULL), m_envmap(NULL),m_rendermanager(NULL), m_accelerator(NULL) {


	m_maxCores = propList.getInteger("cores", -1);
}

Scene::~Scene() {
	delete m_accelerator;
	if (m_sampler)
		delete m_sampler;
	if (m_camera)
		delete m_camera;
	if (m_integrator)
		delete m_integrator;
	if (m_envmap)
		delete m_envmap;
	if (m_rendermanager)
		delete m_rendermanager;
}



void Scene::activate() {
	

	if (!m_integrator)
		throw NoriException("No integrator was specified!");
	if (!m_camera)
		throw NoriException("No camera was specified!");
	
	if (!m_sampler) {
		/* Create a default (independent) sampler */
		m_sampler = static_cast<Sampler*>(
			NoriObjectFactory::createInstance("independent", PropertyList()));
	}

	if(!m_rendermanager)
		m_rendermanager = static_cast<RenderManager*>(
			NoriObjectFactory::createInstance("block", PropertyList()));

	if(!m_accelerator)
		m_accelerator = static_cast<Accelerator*>(
			NoriObjectFactory::createInstance("kdtree", PropertyList()));
	for(int i = 0; i<m_meshes.size();i++)
		m_accelerator->addMesh(m_meshes[i]);
	m_accelerator->build();


	if(m_envmap){
		m_envmap->setParent(this);
	}

	//uncomment this line for assignment 04	
	initLuminaires();

	cout << endl;
	cout << "Configuration: " << qPrintable(toString()) << endl;
	cout << endl;
}

Color3f Scene::sampleRandomLuminaire(LuminaireQueryRecord &lRec, Sampler *sampler)const
{
	float lpdf = 0.0f;
	// random index:
	//int index = (int) (sampler->next1D() * m luminaires.size());
	//lpdf = 1.0 / m luminaires.size();

	// light distribution index:
	Point2f sample = sampler->next2D();
	int index = m_ldistr.sampleReuse(sample.x(), lpdf);
	const Luminaire *l = m_luminaires[index];

	// TODO set lRec sp. possible here?

	return l->sampleDirect(lRec, sample);
}

void Scene::initLuminaires()
{
	this->m_ldistr = DiscretePDF();
	for(int i = 0; i < this->m_luminaires.size(); i++) {
		const Luminaire *l = this->m_luminaires[i];
		float x = l->power().x();
		float y = l->power().y();
		float z = l->power().z();
		//float lpdf = sqrtf(x*x+y*y+z*z);
		float lpdf = l->power().maxCoeff();
		this->m_ldistr.append(lpdf);
	}
	this->m_ldistr.normalize();
}

float Scene::pdfLuminaireDirect(LuminaireQueryRecord &lRec)const{
	if(lRec.lidx>=0){
		int lidx = lRec.lidx;
		float lpdf = m_ldistr[lidx];
		const Luminaire * lum = m_luminaires[lidx];
		return lpdf*lum->pdfDirect(lRec);
	}
	return 0.0;
}

Color3f Scene::sampleLuminairesDirect(Point3f pos,
		LuminaireQueryRecord &lRec, Sampler *sampler)const
{
	float lpdf = 0.0f;
	// random index:
	//int index = (int) (sampler->next1D() * m luminaires.size());
	//lpdf = 1.0 / m luminaires.size();

	// light distribution index:
	Point2f sample = sampler->next2D();
	int index = m_ldistr.sampleReuse(sample.x(), lpdf);
	lRec.lidx = index;
	const Luminaire *l = m_luminaires[index];
	Color3f c = l->sampleDirect(lRec, sampler->next2D());
	lRec.pdf *= lpdf;
	// check if light source is visible
	Ray3f shadowRay = Ray3f(lRec.sp, -lRec.ws_wi);
	shadowRay.mint = Epsilon;
	shadowRay.maxt = lRec.distance *(1.0 - Epsilon);
	shadowRay.update();

	if(this->rayIntersect(shadowRay))
		return Color3f(0.0f);

	return c / lpdf;
}

Color3f Scene::sampleLightRay(LuminaireQueryRecord &lRec, Sampler *sampler) const
{
	// light distribution index:
	float lpdf = 0.0f;
	Point2f sample = sampler->next2D();
	int index = m_ldistr.sampleReuse(sample.x(), lpdf);
	lRec.lidx = index;
	lRec.pdf = lpdf;
	const Luminaire *l = m_luminaires[index];

	return l->sampleLightRay(lRec,sampler);
}


void Scene::addChild(NoriObject *obj) {
	switch (obj->getClassType()) {
		case EMesh: {
				Mesh *mesh = static_cast<Mesh *>(obj);
				m_meshes.push_back(mesh);
				Luminaire* lum = (Luminaire*) mesh->getLuminaire();
				if(lum){
					lum->idx = m_luminaires.size();
					m_luminaires.push_back(lum);
				}
			}
			break;
		
		case ESubScene:
//			obj->setParent(this);
			break;

		case ESampler:
			if (m_sampler)
				throw NoriException("There can only be one sampler per scene!");
			m_sampler = static_cast<Sampler *>(obj);
			break;

		case ECamera:
			if (m_camera)
				throw NoriException("There can only be one camera per scene!");
			m_camera = static_cast<Camera *>(obj);
			break;
		
		case EIntegrator:
			if (m_integrator)
				throw NoriException("There can only be one integrator per scene!");
			m_integrator = static_cast<Integrator *>(obj);
			break;

		case ELuminaire:
			{
			Luminaire *lum = static_cast<Luminaire *>(obj);
			if(lum){
				lum->setParent(this);
				lum->idx = m_luminaires.size();
				m_luminaires.push_back(lum);

			}
			}
			/*
			if (m_envmap)
				throw NoriException("There can only be one environment light per scene!");
			//m_envmap = static_cast<Luminaire *>(obj);
			//doing this in activate, so that we have the scene bbox
			//m_envmap->setParent(this);
			m_luminaires.push_back(m_envmap);
			*/
			break;
		
		case ERenderManager:
			if(m_rendermanager)
				throw NoriException("There can only be one render manager per scene!");
			m_rendermanager = static_cast<RenderManager*>(obj);
			break;
		case EAccelerator:
			if(m_accelerator)
				throw NoriException("There can only be one accelerator per scene!");
			m_accelerator = static_cast<Accelerator*>(obj);
			break;

		default:
			throw NoriException(QString("Scene::addChild(<%1>) is not supported!").arg(
				classTypeName(obj->getClassType())));
	}
}



QString Scene::toString() const {
	QString meshes;
	for (size_t i=0; i<m_meshes.size(); ++i) {
		meshes += QString("  ") + indent(m_meshes[i]->toString(), 2);
		if (i + 1 < m_meshes.size())
			meshes += ",";
		meshes += "\n";
	}
	return QString(
		"Scene[\n"
		"  integrator = %1,\n"
		"  sampler = %2\n"
		"  camera = %3,\n"
		"  medium = %4,\n"
		"  meshes = {\n"
		"  %5}\n"
		"]")
	.arg(indent(m_integrator->toString()))
	.arg(indent(m_sampler->toString()))
	.arg(indent(m_camera->toString()))
	.arg(indent(meshes, 2));
}

NORI_REGISTER_CLASS(Scene, "scene");
NORI_NAMESPACE_END
