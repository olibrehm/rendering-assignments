#include <nori/object.h>

#include <nori/parser.h>
#include <Eigen/Geometry>
#include <QtGui>
#include <QtXml>
#include <QtXmlPatterns>
#include <stack>
#include <nori/scene.h>

NORI_NAMESPACE_BEGIN

class SubScene: public NoriObject{
public:
	SubScene(const PropertyList &propList) {

		m_scene = NULL;
		m_subScene = NULL;
		m_loadFile = false;
		m_filename = propList.getString("filename","");
		if(m_filename.compare("")){
			m_filename = nori::getFullFilePath(m_filename);
			m_loadFile = true;
		}
	}


	void activate(){
	if (m_loadFile){
		NoriParser parser;
		QFile schemaFile(":/schema.xsd");
		QXmlSchema schema;

		#if !defined(PLATFORM_WINDOWS)
			/* Fixes number parsing on some machines (notably those with locale ru_RU) */
			setlocale(LC_NUMERIC, "C");
		#endif

		NoriMessageHandler handler;
		schema.setMessageHandler(&handler);
		if (!schemaFile.open(QIODevice::ReadOnly))
			throw NoriException("Unable to open the XML schema!");
		if (!schema.load(schemaFile.readAll()))
			throw NoriException("Unable to parse the XML schema!");

		QXmlSchemaValidator validator(schema);
		QFile file(m_filename);
		if (!file.open(QIODevice::ReadOnly))
			throw NoriException(QString("Unable to open the file \"%1\"").arg(m_filename));
		if (!validator.validate(&file))
			throw NoriException(QString("Unable to validate the file \"%1\"").arg(m_filename));


		QXmlInputSource source(&file);
		file.seek(0);
		QXmlSimpleReader reader;
		reader.setContentHandler(&parser);
		if (!reader.parse(source)) 
			throw NoriException(QString("Unable to parse the file \"%1\"").arg(m_filename));
		
		m_subScene = static_cast<SubScene*>(parser.getRoot());
		}
				
	}

	void setParent(NoriObject *obj) {
		m_scene = static_cast<Scene*>(obj);
		if(m_subScene!=NULL){
			m_subScene->setParent(obj);
		}

		for(int i=0;i<m_objs.size();i++){
			m_scene->addChild(m_objs[i]);
		}
	}


	 void addChild(NoriObject *child){

		 m_objs.push_back(child);
		
	 }


	/// Return a human-readable summary
	QString toString() const {
		return QString(
			"SubScene[\n"
			"]");
	}

	EClassType getClassType() const { return ESubScene; }


private:
	QString m_filename;
	bool m_loadFile;
	Scene* m_scene;

	SubScene* m_subScene;

	std::vector<NoriObject*> m_objs;

};

NORI_REGISTER_CLASS(SubScene, "xml");
NORI_NAMESPACE_END