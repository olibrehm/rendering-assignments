#include <nori/imgblock.h>
#include <nori/rfilter.h>
//#include <nori/camera.h>
#include <nori/bitmap.h>

#include <nori/bbox.h>
NORI_NAMESPACE_BEGIN

ImageBlock::ImageBlock(const Vector2i &size, const ReconstructionFilter *filter) 
		: m_offset(0), m_size(size) {
	/* Tabulate the image reconstruction filter for performance reasons */
	m_filterRadius = filter->getRadius();
	m_borderSize = (int) std::ceil(m_filterRadius - 0.5f);
	m_filter = new float[NORI_FILTER_RESOLUTION + 1];
	for (int i=0; i<NORI_FILTER_RESOLUTION; ++i) {
		float pos = (m_filterRadius * i) / NORI_FILTER_RESOLUTION;
		m_filter[i] = filter->eval(pos);
	}
	m_filter[NORI_FILTER_RESOLUTION] = 0.0f;
	m_lookupFactor = NORI_FILTER_RESOLUTION / m_filterRadius;
	m_weightsX = new float[(int) std::ceil(2*m_filterRadius) + 1];
	m_weightsY = new float[(int) std::ceil(2*m_filterRadius) + 1];

	/* Allocate space for pixels and border regions */
	resize(size.y() + 2*m_borderSize, size.x() + 2*m_borderSize);
}

ImageBlock::~ImageBlock() {
	delete[] m_filter;
	delete[] m_weightsX;
	delete[] m_weightsY;
}

Bitmap *ImageBlock::toBitmap() const {
	Bitmap *result = new Bitmap(m_size);
	for (int y=0; y<m_size.y(); ++y)
		for (int x=0; x<m_size.x(); ++x)
			result->coeffRef(y, x) = coeff(y + m_borderSize, x + m_borderSize).normalized();
	return result;
}

void ImageBlock::put(const Point2f &_pos, const Color3f &value) {
	if (!value.isValid()) {
		/* If this happens, go fix your code instead of removing this warning ;) */
		cerr << "Integrator: computed an invalid radiance value: " 
			 << qPrintable(value.toString()) << endl;
		return;
	}

	/* Convert to pixel coordinates within the image block */
	Point2f pos(
		_pos.x() - 0.5f - (m_offset.x() - m_borderSize),
		_pos.y() - 0.5f - (m_offset.y() - m_borderSize)
	);

	/* Compute the rectangle of pixels that will need to be updated */
	BoundingBox2i bbox(
		Point2i( std::ceil(pos.x() - m_filterRadius),  std::ceil(pos.y() - m_filterRadius)),
		Point2i(std::floor(pos.x() + m_filterRadius), std::floor(pos.y() + m_filterRadius))
	);
	bbox.clip(BoundingBox2i(Point2i(0, 0), Point2i(cols() - 1, rows() - 1)));

	/* Lookup values from the pre-rasterized filter */
	for (int x=bbox.min.x(), idx = 0; x<=bbox.max.x(); ++x)
		m_weightsX[idx++] = m_filter[(int) (std::abs(x-pos.x()) * m_lookupFactor)];
	for (int y=bbox.min.y(), idx = 0; y<=bbox.max.y(); ++y)
		m_weightsY[idx++] = m_filter[(int) (std::abs(y-pos.y()) * m_lookupFactor)];
	
	/* spreads the value over all pixel in the region of the filter size */
	for (int y=bbox.min.y(), yr=0; y<=bbox.max.y(); ++y, ++yr) 
		for (int x=bbox.min.x(), xr=0; x<=bbox.max.x(); ++x, ++xr) 
			coeffRef(y, x) += Color4f(value) * m_weightsX[xr] * m_weightsY[yr];
}
	
void ImageBlock::putNoFilter(const Point2f &_pos, const Color3f &value) {
	if (!value.isValid()) {
		/* If this happens, go fix your code instead of removing this warning ;) */
		cerr << "Integrator: computed an invalid radiance value: " 
			 << qPrintable(value.toString()) << endl;
		return;
	}

	/* Convert to pixel coordinates within the image block */
	Point2f pos(
		_pos.x() - 0.5f - (m_offset.x() - m_borderSize),
		_pos.y() - 0.5f - (m_offset.y() - m_borderSize)
	);

	Point2i ipos = Point2i(std::ceil(pos.x()), std::ceil(pos.y()));

	coeffRef(ipos.y(),ipos.x()) += Color4f(value); 

	/* Compute the rectangle of pixels that will need to be updated */
/*
	BoundingBox2i bbox(
		Point2i( std::ceil(pos.x() - m_filterRadius),  std::ceil(pos.y() - m_filterRadius)),
		Point2i(std::floor(pos.x() + m_filterRadius), std::floor(pos.y() + m_filterRadius))
	);
	bbox.clip(BoundingBox2i(Point2i(0, 0), Point2i(cols() - 1, rows() - 1)));
*/
	/* Lookup values from the pre-rasterized filter */
/*
	for (int x=bbox.min.x(), idx = 0; x<=bbox.max.x(); ++x)
		m_weightsX[idx++] = m_filter[(int) (std::abs(x-pos.x()) * m_lookupFactor)];
	for (int y=bbox.min.y(), idx = 0; y<=bbox.max.y(); ++y)
		m_weightsY[idx++] = m_filter[(int) (std::abs(y-pos.y()) * m_lookupFactor)];
*/	
	/* spreads the value over all pixel in the region of the filter size */
/*
	for (int y=bbox.min.y(), yr=0; y<=bbox.max.y(); ++y, ++yr) 
		for (int x=bbox.min.x(), xr=0; x<=bbox.max.x(); ++x, ++xr) 
			coeffRef(y, x) += Color4f(value) * m_weightsX[xr] * m_weightsY[yr];
*/
}

void ImageBlock::put(ImageBlock &b) {
	Vector2i offset = b.getOffset() - m_offset;
	Vector2i size   = b.getSize()   + Vector2i(2*b.getBorderSize());
	block(offset.y(), offset.x(), size.y(), size.x()) 
		+= b.topLeftCorner(size.y(), size.x());
}

QString ImageBlock::toString() const {
	return QString("ImageBlock[offset=%1, size=%2]]")
		.arg(m_offset.toString())
		.arg(m_size.toString());
}


NORI_NAMESPACE_END