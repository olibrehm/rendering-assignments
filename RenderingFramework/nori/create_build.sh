#!/bin/bash
rm -rdf build
mkdir build
cd build
mkdir Release
cd Release 
cmake -DCMAKE_BUILD_TYPE=Release ../../
cd ..

mkdir Debug
cd Debug 
cmake -DCMAKE_BUILD_TYPE=Debug ../../
cd ..


