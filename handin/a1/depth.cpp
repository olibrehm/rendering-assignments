Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
...
    float depth = its.t;
    if( depth > m_maxDepth )
        depth = m_maxDepth;
    if( depth < m_minDepth )
        depth = m_minDepth;
    depth = (depth - m_minDepth) / (m_maxDepth - m_minDepth);
    Color3f cd = Color3f(depth);
    return cd;
}
