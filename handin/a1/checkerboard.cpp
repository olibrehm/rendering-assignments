
CheckerBoardTexture2D::Color3f eval(const Intersection &its ,bool filter = true) const
{
    Point2f uv = getFinalUV(its.uv);
    int x = (int) (m_tiles * uv[0]);
    int y = (int) (m_tiles * uv[1]);
    bool c0;

    if( x%2 != 0 )
    {
        if( y%2 != 0 )
            c0 = true;
        else
            c0 = false;
    }
    else
    {
        if( y%2 != 0 )
            c0 = false;
        else
            c0 = true;
    }

    return c0 ? m_color0 : m_color1;
}


