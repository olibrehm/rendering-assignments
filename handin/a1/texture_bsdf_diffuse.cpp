
class Diffuse : public BSDF {
public:
...
    Color3f eval( const BSDFQueryRecord &bRec ) const
    {
        ...
        Color3f c;
        if( m_albedoTexture != NULL )
            c = m_albedoTexture->eval( bRec.its, false );
        else
            c = m_albedo;
        /* The BRDF is simply the albedo / pi */
        return c * INV_PI;
    }

    void addChild( NoriObject *child )
    {
        if( child->getClassType() == ETexture )
            m_albedoTexture = static_cast<Texture2D*>( child );
    }
...
private:
    Color3f m_albedo;
    Texture2D* m_albedoTexture;
};
